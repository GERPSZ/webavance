<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :    GPS                      *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");

?>

           
			<div class="main">
            
	    
			<header class="codrops-header gribverte">  
            <div id="colortitle">  
			<h1> La Gribouille Verte </h1>	
            </div>
			</header>
                
           <div class="maincontent">
          
            <h3>Gestes verts pour  une Gribouille verte</h3>
    <p>&Agrave; La Gribouille, nous sommes conscients que chaque petit geste compte  et, au quotidien, nous tentons de faire notre part. Que ce soit de fa&ccedil;on  &eacute;ducative aupr&egrave;s des enfants ou par des choix organisationnels (les achats et  la gestion du milieu), nous avons pris de petites habitudes vertes. En voici  quelques-unes&nbsp;:<br />
      <br />
    </p>
    <ul>
      <li>L&rsquo;utilisation de savon &agrave; vaisselle biod&eacute;gradable;</li>
      <li>L&rsquo;emploi de nettoyant tout usage biod&eacute;gradable;</li>
      <li>L&rsquo;usage du savon &agrave; main pur (savon doux,  hypoallerg&egrave;ne et biod&eacute;gradable);</li>
      <li>L&rsquo;utilisation de sacs de plastique biod&eacute;gradables  pour les v&ecirc;tements souill&eacute;s;</li>
      <li>&Eacute;teindre les lumi&egrave;res lorsqu&rsquo;on sort du local et  baisser le chauffage la nuit;</li>
      <li>Accompagner les enfants &agrave; r&eacute;duire leur  consommation d&rsquo;eau lors du lavage des mains;</li>
      <li>R&eacute;utiliser divers mat&eacute;riaux (retailles de papier,  contenants, bo&icirc;tes, etc.);</li>
      <li>Utilisation d&rsquo;objets r&eacute;els (t&eacute;l&eacute;phone qui ne  fonctionne plus, horloge, objets usuels, etc.);</li>
      <li>Recycler les mati&egrave;res recyclables;</li>
      <li>Participation des enfants au projet des terrasses  verte du C&eacute;gep;</li>
      <li>Utilisation de vaisselle r&eacute;utilisable pour les  repas;</li>
      <li>Et encore plus &agrave; venir...</li>
    </ul>
    <p>Pour vous inspirer, je vous r&eacute;f&egrave;re au reportage de <a href="http://vieenvert.telequebec.tv/occurrence.aspx?id=569" title="La Vie en vert" target="_blank" class="hyper">La Vie en  vert</a>.<br />
      Visitez le site <a href="http://www.evb.csq.qc.net" target="_blank" class="hyper">Bruntland</a> pour un milieu plus vert.<br />
      Consultez le <em>Guide du CPE &eacute;co-responsable</em> au  <a href="http://www.cpedurable.org" title="Guide du CPE &eacute;co-responsable" target="_blank" class="hyper">www.cpedurable.org</a> <br />
      Un personnage qui sensibilise les enfants, <em><a href="http://books.google.ca/books?id=tb1FYZOusioC&amp;printsec=frontcover&amp;dq=l%C3%A9on+et+l'environnement&amp;source=bl&amp;ots=mlnVuWlluT&amp;sig=akS4oRVDD5-1W0dwKm5WtJykHFQ&amp;hl=fr&amp;ei=xrfqS_jSIMGblgezsZScBA&amp;sa=X&amp;oi=book_result&amp;ct=result&amp;resnum=3&amp;ved=0CCQQ6AEwAg#v=onepage&amp;q&amp;f=false" title="L&eacute;on et l'environnement" target="_blank" class="hyper">L&eacute;on et l&rsquo;environnement</a>.</em> </p>
    <p>&nbsp;</p>
<p style="text-align:center;">&nbsp;</p>
	  <p style="text-align:center;">&nbsp;</p>
            
            
            
            	
      </div>
      </div>
            

<?php
    
    

?>