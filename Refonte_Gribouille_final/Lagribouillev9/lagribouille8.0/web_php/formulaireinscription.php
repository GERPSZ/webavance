<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */


   require_once("securimage/securimage.php");
   $img = new securimage();
   $valido_captcha = $img->check($_POST['captchacode']);

  if ($valido_captcha)
  {
       session_destroy();
      
       require_once("action/FormulaireAction.php");
       $action = new FormulaireAction();
       $v = $action->execute();
  
}else
{
   echo "********captcha invalide ******";
}


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

	<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        
        
		<title>La Gribouille | Sidebar menu</title>
       
      
         <div class="headergribouille">
         <img src="img/header.jpg" alt="logo" width="1000"/>
         </div>
        
         <p class="menuhaut">
         <a href="index.php" class="menuhaut">Accueil</a>
         <a href="plan.php" class="menuhaut">Plan du site</a>
         <a href="nousjoindre.php" class="menuhaut">Nous joindre</a>
         </p>
         
         
		
		
		<link rel="stylesheet" property="stylesheet" type="text/css" href= "../fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
		<link rel="stylesheet" property="stylesheet" type="text/css" href="css/demoadmin.css" />
		<link rel="stylesheet" property="stylesheet" type="text/css" href="css/sidebar_formulaire.css" />

    <script type="text/javascript" src="js/scriptvalidationparents.js"></script>    
       
		<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	    </head>
	    <body onload="remplirTouteDate('dateJour', 'dateMois', 'dateAnnee');">
		    
      <div class="container">
			<div class="main">
            
		<header class="codrops-header">    
			<h1> FORMULAIRE D'INSCRIPTION </h1>	
		</header>
                
        <div class="maincontent">     
     
       <form id="form" method="post">
     

          <div class="papa">
                <input type="hidden" name="lang" value="en" />
                    
                    <div class="espace"> 
				   	<p>Nom du père</p>
                    <p>Téléphone Cellulaire</p>
                    <p>Téléphone au Travail</p>
                    <p>Téléphone à la maison</p>
                    <p>Adresse</p>
                    <p>Code Postal</p>
                    <p>Ville</p>
                    <p>Adresse courriel</p>
                    </div>
                    
                     <div class="information"> 
                    <input type="text" class="pencil" id="namepere" name="namepere" placeholder="Nom du père" >
                    <input type="text" class="pencil" id="telephonecel" name="telephonecel" placeholder="Téléphone Cellulaire">
                    <input type="text" class="pencil" id="telephonetravail" name="telephonetravail" placeholder="Your telephone">
                    <input input type="text" class="pencil" id="telephonemaison" name="telephonemaison" placeholder="telephone travail">
                    <input input type="text" class="pencil" id="adresse" name="adresse" placeholder="Adresse">
                    <input type="text" class="pencil" id="postal" name="postal" placeholder="Code Postal">
                    <input  type="text" class="pencil" id="ville" name="ville" placeholder="Ville">
                    <input type="email" class="pencil" id="email" name="email" placeholder="Your email">  
                    </div>

                  

         <div id="cvm"> 
             
          <p><label>Status du ou des parents:&nbsp;</label>
          <p><label>Étudiant au cégep du Vieux Montréal&nbsp;</label>
          <br/>
          
          <input name="etudiantjour" type="checkbox" value="1"/>Jour
          <br/>
         <input name="etudiantsoir" type="checkbox" />Soir
          <br/>
          <br/>
          <br/>
          <input name="employecvm" type="checkbox" />Employé au Cégep du Vieux Montréal 
          </div>
         </div>
         
          <div class="mama">
          
                <input type="hidden" name="lang" value="en" />
                    
                 <div class="espace"> 
				   	<p>Nom de la mère</p>
                    <p>Nom de l'enfant</p>
                    <p>Date de naissance</p>
                    <p>Téléphone Cellulaire</p>
                    <p>Téléphone au Travail</p>
                    <p>Téléphone à la maison</p>
                    <p>Adresse</p>
                    <p>Code Postal</p>
                    <p>Ville</p>
                    <p>Adresse courriel</p>
                 </div>
                    
                     <div class="information"> 
                    <input type="text" class="pencil2" id="namemere" name="namemere" placeholder="Nom de la mère" >
                    <input type="text" class="pencil2" id="namenfant" name="namenfant" placeholder="Nom de l'enfant" >
                    
                    <div class="loginInput pencil2">
                    
								<select name="dateJour" class="dateJ">
									<!--<script>remplirDate(1, 31);</script>-->
								</select>

								<select name="dateMois" class="dateM">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
								</select>

								<select name="dateAnnee" class="dateA">
									<option value="1991">1991</option>
									<option value="1992">1992</option>
									<option value="1993">1993</option>
								</select>
					</div>
                    
                    
                    <input type="text" class="pencil2" id="telephonecelmere" name="telephonecelmere" placeholder="Téléphone Cellulaire">
                    <input type="text" class="pencil2" id="telephonetravailmere" name="telephonetravailmere" placeholder="Téléphone au Travail">
                    <input input type="text" class="pencil2" id="telephonemaisonmere" name="telephonemaisonmere" placeholder="Téléphone à la maison">
                    <input input type="text" class="pencil2" id="adressemere" name="adressemere" placeholder="Adresse">
                    <input type="text" class="pencil2" id="codpostalmere" name="codpostalmere" placeholder="Code Postal">
                    <input  type="text" class="pencil2" id="villemere" name="villemere" placeholder="Ville">
                    <input type="email" class="pencil2" id="emailmere" name="emailmere" placeholder="Your email">  
                    </div>
                    
         <div id="cvm"> 
             
          <p><label>Status du ou des parents:&nbsp;</label>
          <p><label>Étudiant au cégep du Vieux Montréal&nbsp;</label>
          <br/>
          <input name="etudiantjourmere" type="checkbox" />Jour
          <br/>
         <input name="etudiantsoirmere" type="checkbox" />Soir
          <br/>
          <br/>
          <br/>
          <input name="employecvmmere" type="checkbox" />Employé au Cégep du Vieux Montréal 
          </div>
         </div>
            

          <img src="securimage/securimage_show.php" id="captcha" />
          <input type="text" name="captchacode" />


         <div id="buttons">
                    <input class="buttonstyle" type="submit" id="bsend" name="submit" Value="SEND"/>
                    <a href="#" class="divider"></a>   
                    <a href="index.php" class="buttonstylequit"/>SORTIR &nbsp;✘</a>
        </div>



			   </form>  
          </div>

          <div class="content">
			   	</div>       	
          </div>
              
				<!-- Related demos -->
	   			<section class="related">	
		   		</section>
                
			</div><!-- /main -->
                 
		
        
		
        
	</body>
</html>
<?php




?>