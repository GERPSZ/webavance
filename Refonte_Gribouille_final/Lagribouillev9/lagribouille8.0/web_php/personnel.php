<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  	
?>

          
		   	<div class="main">
            
	    
                
           <div class="maincontent">
          
           <h1>Le personnel de La Gribouille</h1>
<p>Dipl&ocirc;m&eacute; en TEE, le personnel &eacute;ducateur de La Gribouille respecte et applique le programme &eacute;ducatif des services de garde du Qu&eacute;bec.</p>
<h3>M&eacute;lanie Claudio</h3>
<p>M&eacute;lanie est &eacute;ducatrice &agrave; La Gribouille, personne-ressource pour les &eacute;tudiants et personne responsable du soutien p&eacute;dagogique pour les enseignants des programmes de Techniques d'&eacute;ducation &agrave; l'enfance et de Techniques d'&eacute;ducation sp&eacute;cialis&eacute;e.</p>
<p>En 1998, elle a obtenu son dipl&ocirc;me en Techniques d&rsquo;&eacute;ducation &agrave; l&rsquo;enfance au c&eacute;gep du Vieux Montr&eacute;al. Elle a exerc&eacute; la profession d&rsquo;&eacute;ducatrice pendant 8 ans dans un centre de la petite enfance avant d'obtenir, en 2004, un poste &agrave; La Gribouille. </p>
<p>&Agrave; l'&eacute;t&eacute; de 2007, M&eacute;lanie enseigne un programme de formation destin&eacute; aux futurs responsables de service de garde. </p>
<p>Passionn&eacute;e de la petite enfance, M&eacute;lanie a obtenu un certificat dans les programmes Petite enfance et famille : intervention pr&eacute;coce et Intervention psycho&eacute;ducative. Depuis 2008, elle poursuit des &eacute;tudes &agrave; l&rsquo;UQAM au baccalaur&eacute;at en Enseignement de la formation professionnelle et technique.</p>
<p><img src="img/images/Melanie3.jpg" width="660" height="508" alt="M&eacute;lanie Claudio"/></p>
<h3>Fran&ccedil;ois Couture</h3>
<p>Fran&ccedil;ois est &eacute;ducateur &agrave; La Gribouille, personne-ressource pour les &eacute;tudiants et responsable du soutien p&eacute;dagogique pour les enseignants des programmes Techniques d&rsquo;&eacute;ducation &agrave; l&rsquo;enfance et Techniques d'&eacute;ducation sp&eacute;cialis&eacute;e. Il a travaill&eacute; deux ans et demi au laboratoire comme &eacute;ducateur avant d&rsquo;y obtenir un poste en 2008.</p>
<p>Titulaire de deux DEC au c&eacute;gep du Vieux Montr&eacute;al (Sciences humaines, 2002, et Techniques d'&eacute;ducation &agrave; l'enfance, 2005) il a entam&eacute; des &eacute;tudes universitaires en Philosophie et &eacute;tudes f&eacute;ministes &agrave; l&rsquo;UQAM. Par la suite, il s'est dirig&eacute; vers le baccalaur&eacute;at en Enseignement de la formation professionnelle et technique.</p>
<p>Ce choix a &eacute;t&eacute; motiv&eacute; par une exp&eacute;rience d&rsquo;enseignement aux adultes dans le cadre du programme de formation obligatoire pour les responsables de garde en milieu familial &agrave; l&rsquo;&eacute;t&eacute; de 2007. </p>
<p>De plus, Fran&ccedil;ois est intervenant au GRIS Montr&eacute;al (Groupe de recherche et d&rsquo;intervention sociale) depuis 2006.</p>
<p><img src="img/images/Francois3.jpg" width="660" height="508" alt="Fran&ccedil;ois Couture" /><br />
</p>
<p></p>
<p style="text-align:center;">&nbsp;</p>
	  <p style="text-align:center;">&nbsp;</p>
            
            
            	
          </div>
          </div>
            
                

                
		
                 
	  
        
		<script src="../js/classie.js"></script>
        
        
	</body>
</html>

<?php




?>