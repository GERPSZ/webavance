<?php

  	require_once("action/ExtraireCitationsAction.php");
  	$action = new ExtraireCitationsAction();
    $action->execute();

    $arraymessages = array();
    $arraymessages = $action->getarray();

    $arrayquatre = array();

    for ($x = 0; $x <=3; $x++) 
        {
           $randkey = array_rand($arraymessages);
           $value = $arraymessages[$randkey];
           array_push($arrayquatre, $value);
           
        } 

   	 
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">


	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        
        
		<title><?php  #echo $GLOBALS['title']; ?></title>
       
		<link rel="stylesheet"  property="stylesheet" type="text/css" href="fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
		<link rel="stylesheet"  property="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet"  property="stylesheet" type="text/css" href="css/sidebar.css" />
        <link rel="stylesheet"  property="stylesheet" type="text/css" href="css/menudes.css" />
        
        
		<script src="js/snap.svg-min.js"></script>
		<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
    
	<body>

        <div class="headergribouille">
         <img src="img/header.jpg" alt="logo" width="1000"/>
         </div>
        
         <p class="menuhaut">
         <a href="index.php" class="menuhaut">Accueil</a>
         <a href="plan.php" class="menuhaut">Plan du site</a>
         <a href="nousjoindre.php" class="menuhaut">Nous joindre</a>
         </p>


          <link rel="stylesheet"  property="stylesheet" type="text/css" href="fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
    <link rel="stylesheet"  property="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet"  property="stylesheet" type="text/css" href="css/sidebar.css" />
    <link rel="stylesheet"  property="stylesheet" type="text/css" href="css/menudes.css" />
        
        
    <script src="js/snap.svg-min.js"></script>
    <!--[if IE]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
    
  <body>
    <div class="container">
        <div class="menudes">
          <ul>
            <li><a href="milieu.php"><em class="fa fa-fw fa-home fa-2x"></em><span> Milieu de garde</span></a></li>
            <li><a href="programme.php"><i class="fa fa-mortar-board fa-2x"></i><span> Programme éducatif</span></a></li>
            <li><a href="labo.php"><i class="fa fa-fw fa-bus fa-2x"></i><span> Laboratoire d'observation</span></a></li>
            <li><a href="equipe.php"><i class="fa fa-fw fa-group fa-2x"></i><span> Équipe</span></a></li>
            <li><a href="parents.php"><i class="fa fa-fw fa-heart fa-2x"></i><span> Place des parents</span></a></li>
           <li><a href="liens.php"><i class="fa fa-fw fa-align-center fa-2x"></i><span> Liens Utiles</span></a></li>
                        <li><a href="gribverte.php"><i class="fa fa-fw fa-tree fa-2x" style="color:green;"></i><span> Gribouille Verte</span></a></li>
                        <li><a href="administration.php"><i class="fa fa-fw fa-tachometer fa-2x" style="color:red;"></i><span> Administrateur</span></a></li>
                         <li><a href="pageparents.php"><i class="fa fa-fw fa-smile-o fa-2x" style="color:red;"></i><span> Espace parents</span></a></li>
                          <li><a href="formulaireinscription.php"><i class="fa fa-fw fa-archive fa-2x" style="color:red;"></i><span> Formulaire d'nscription</span></a></li>
                        
          </ul>
        </div>


              <div  class="messagesenfants">  
            <table>
               <?php  for($x = 0; $x<4; $x++)  { ?>
                    <?php   foreach($arrayquatre[$x] as $key=>$val) { ?>
                    <tr>
                      <?php $message = (strip_tags($key)); ?>
                      <?php $auteur = (strip_tags($val)); ?>
                      <td class="mess"><?php echo $message ?></td>
                      <tr></tr>

                      <td class="autor"><?php echo $auteur ?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                   </tr>
                   <?php } ?>
              <?php } ?>  
            </table>
          </div>