<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
   
?>

			<div class="main">
            
			
      <header class="codrops-header titleprogramme">    
			<h1>Laboratoire d'observation </h1>	
			</header>
                
      <div class="maincontent">
      <div class="photoedu">      
      <a href="http://www.mfa.gouv.qc.ca/fr/publication/Documents/programme_educatif.pdf"><img src="img/images/labo.jpg"  width="500" alt="Programme éducatif des services de garde du Québec"/></a>
      </div>
      	
						
				  		<ul>
							<li><a href="roleeducateurs.php"><i class="fa fa-fw fa-language"></i>Rôle des éducateurs</a></li>
							<li><a href="ethique.php"><i class="fa fa-fw fa-sort-alpha-asc"></i>Éthique d'observation</a></li>	
					  	</ul>
				
				
            <p>La Gribouille porte de nombreux chapeaux : c&rsquo;est un milieu de garde pour les parents &eacute;tudiants; c&rsquo;est une laboratoire d&rsquo;observation pour les &eacute;tudiants et les enseignants des  techniques humaines; c&rsquo;est &eacute;galement une ressource didactique gr&acirc;ce aux interactions entre le personnel &eacute;ducateur et les &eacute;tudiants.</p>
  <p>Pour observer le quotidien de La Gribouille, il suffit de s&rsquo;installer dans la salle attenante au local o&ugrave; se trouvent les enfants. Ce local en forme de &laquo;L&raquo; est dot&eacute; d&rsquo;une trentaine de si&egrave;ges munis de postes d&rsquo;&eacute;coute individuels. Afin de s&rsquo;assurer que les observateurs ne g&ecirc;nent pas les enfants, deux murs du local sont dot&eacute;s de miroirs sans tain. C&rsquo;est ainsi qu&rsquo;&agrave; chaque session, des centaines d&rsquo;heures d&rsquo;observation permettent aux futurs &eacute;ducateurs de se familiariser avec le monde de la petite enfance.</p>
  <p>Des enseignants de Technique d&rsquo;&eacute;ducation &agrave; l&rsquo;enfance disent qu&rsquo;il est plut&ocirc;t exceptionnel de retrouver un lieu o&ugrave; l&rsquo;on  peut&nbsp;:</p>
  <ul>
  <li>faire observer aux  &eacute;l&egrave;ves l&rsquo;application au quotidien de toute la th&eacute;orie enseign&eacute;e dans la formation;<strong><u></u></strong></li>
  <li>discuter de p&eacute;dagogie  avec le personnel &eacute;ducateur;<strong><u></u></strong></li>
  <li>favoriser le soutien  aux comp&eacute;tences parentales chez des parents &eacute;tudiants dans un c&eacute;gep;</li>
  <li>permettre aux jeunes parents  de retourner aux &eacute;tudes;</li>
  <li>accompagner des  enfants &acirc;g&eacute;s de 2 &agrave; 5 ans dans leur d&eacute;veloppement.</li>
  </ul>
  
  
	<h3>La Gribouille est une ressource p&eacute;dagogique parce qu&rsquo;elle offre&nbsp;:<br /></h3>
  <ul>
  <li>un soutien &agrave; l&rsquo;enseignement pour les enseignants du CVM;</li>
  <li>un soutien &agrave; l&rsquo;apprentissage pour les &eacute;tudiants des  techniques d&rsquo;&Eacute;ducation &agrave; l&rsquo;enfance (TEE) et d&rsquo;&Eacute;ducation sp&eacute;cialis&eacute;e (TES);</li>
  <li>un soutien pour certains travaux scolaires,  notamment en design, en arts plastiques et en photographie; &nbsp;</li>
  <li>une communication active avec le personnel  &eacute;ducateur; &nbsp;</li>
  </ul>
  <h3>Mais qu&rsquo;est-ce que les &eacute;tudiants peuvent bien observer &agrave; La Gribouille&nbsp;?</h3>
  <p>&Agrave; La Gribouille, on observe  principalement les enfants  &acirc;g&eacute;s de 2 &agrave; 5 ans ainsi que le personnel  &eacute;ducateur.</p>
  <P>Ces mod&egrave;les  d&rsquo;&eacute;ducateurs sont n&eacute;cessaires pour &eacute;tablir les liens entre la th&eacute;orie et la  pratique. Ce que nous permet La Gribouille, c'est p&eacute;n&eacute;trer dans la  r&eacute;alit&eacute; d&rsquo;un groupe d&rsquo;enfants, observer comment l&rsquo;&eacute;ducateur intervient, anime,  d&eacute;veloppe l&rsquo;estime de soi et l&rsquo;autonomie des enfants afin que ceux-ci  s&rsquo;&eacute;panouissent pleinement &agrave; travers le jeu.</p>
  <p>Pour plus d'information, visitez le site du programme<br />
  <a href="http://www.cvm.qc.ca/formationreg/programmes/educenfance/Pages/index.aspx" target="_blank" class="hyper"><strong>Techniques d'&eacute;ducation &agrave; l'enfance</strong></a> (TEE).</p>
  <p>&nbsp;</p>
                 
                 
                 
      </div>
                 
                 
			
<?php

  

?>