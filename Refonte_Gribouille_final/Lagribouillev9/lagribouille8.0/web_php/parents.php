<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  	
?>

            
			<div class="main">
            
		<header class="codrops-header placeparents">    
			<h1> La place des parents </h1>	
			</header>
                
        <div class="maincontent">
          
           <p>&Agrave; La Gribouille, nous encourageons et participons au soutien des comp&eacute;tences parentales en organisant diff&eacute;rentes rencontres individuelles ou des ateliers parents-enfants. Quelques parents &eacute;tudiant au CVM nous informent que La Gribouille contribue largement &agrave; la poursuite de leurs &eacute;tudes et qu&rsquo;ils appr&eacute;cient le fait de b&eacute;n&eacute;ficier du temps et de la confiance n&eacute;cessaires pour se concentrer sur leurs &eacute;tudes. Cela leur permet aussi d&rsquo;&ecirc;tre plus disponibles le soir dans leur vie familiale.</p>
           <p>Les parents de La Gribouille peuvent, &agrave; tout moment de la journ&eacute;e, observer leurs enfants en interaction avec leurs pairs et avec d&rsquo;autres adultes en utilisant la salle d&rsquo;observation du local 8.56.</p>

            	
       </div>
       </div>
            
        
		<script src="../js/classie.js"></script>
        
		   
	</body>
</html>
<?php




?>