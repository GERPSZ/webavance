<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  
?>

          
			<div class="main">
            
			
                
           <div class="maincontent">
           <h1>Le d&eacute;veloppement global de l'enfant</h1>
         
<p>Le d&eacute;veloppement de l&rsquo;enfant est un processus global qui fait appel &agrave; plusieurs dimensions. Chacune d&rsquo;elles intervient toutefois &agrave; des degr&eacute;s divers, selon les apprentissages de l&rsquo;enfant et les activit&eacute;s auxquelles il s&rsquo;adonne.</p>
<h3>La dimension affective</h3>
<p>La satisfaction des besoins affectifs de l&rsquo;enfant est tout aussi vitale que celle de ses besoins physiques. Aussi est-il de la plus haute importance de cr&eacute;er une relation affective stable et s&eacute;curisante avec l&rsquo;enfant d&egrave;s son entr&eacute;e au service de garde, car c&rsquo;est &agrave; partir de cette relation qu&rsquo;il pourra se d&eacute;velopper harmonieusement.</p>
<h3>La dimension physique et motrice</h3>
<p>Cette dimension fait r&eacute;f&eacute;rence aux besoins physiologiques, physiques, sensoriels et moteurs de l&rsquo;enfant. Le d&eacute;veloppement de ses habilet&eacute;s motrices (agilit&eacute;, endurance, &eacute;quilibre, lat&eacute;ralisation, etc.) comprend la motricit&eacute; globale (s&rsquo;asseoir, ramper, marcher, courir, grimper, saisir un objet, etc.) et la motricit&eacute; fine (dessiner, enfiler des perles, d&eacute;couper, etc.). Offrir aux enfants la possibilit&eacute; de bouger en service de garde favorise leur d&eacute;veloppement physique et moteur tout en les menant &agrave; acqu&eacute;rir de saines habitudes de vie et en pr&eacute;venant l&rsquo;ob&eacute;sit&eacute;.</p>
<h3>La dimension sociale et morale</h3>
<p>Le milieu de garde offre &agrave; l&rsquo;enfant l&rsquo;occasion d&rsquo;apprendre &agrave; entrer en relation avec d&rsquo;autres, &agrave; exprimer et &agrave; contr&ocirc;ler ses &eacute;motions, &agrave; se mettre &agrave; la place de l&rsquo;autre et &agrave; r&eacute;soudre des probl&egrave;mes. L&rsquo;acquisition d&rsquo;habilet&eacute;s sociales et l&rsquo;&eacute;mergence d&rsquo;une conscience du bien et du mal lui permettent d&rsquo;entretenir des  relations de plus en plus harmonieuses avec son entourage et de tenir compte de la perspective des autres avant d&rsquo;agir.</p>
<h3>La dimension cognitive</h3>
<p>Un milieu de vie stimulant permet &agrave; l&rsquo;enfant de d&eacute;velopper ses sens, d&rsquo;acqu&eacute;rir des connaissances et des habilet&eacute;s nouvelles et de comprendre de plus en plus le monde qui l&rsquo;entoure. Le personnel &eacute;ducateur et les RSG soutiennent les enfants sur ce plan en favorisant chez eux la r&eacute;flexion, le raisonnement et la cr&eacute;ativit&eacute;.</p>
<h3>La dimension langagi&egrave;re</h3>
<p>Le d&eacute;veloppement du langage et de la repr&eacute;sentation symbolique est renforc&eacute; par la vie en groupe. Le personnel des services de garde contribue au d&eacute;veloppement des enfants sur ce plan en parlant avec eux et en les aidant &agrave; exprimer de mieux en mieux leurs besoins et leurs &eacute;motions, &agrave; poser des questions, &agrave; am&eacute;liorer leur prononciation et leur vocabulaire.</p>
<p>Source&nbsp;: minist&egrave;re de la Famille et des A&icirc;n&eacute;s, <em>Accueillir  la petite enfance</em>, le programme &eacute;ducatif des services de garde du Qu&eacute;bec, mise  &agrave; jour de 2007.</p>
<p style="text-align:center;">&nbsp;</p>
	  <p style="text-align:center;">&nbsp;</p>
            
                 	
          </div>
          </div>
            
                
<?php

 

?>