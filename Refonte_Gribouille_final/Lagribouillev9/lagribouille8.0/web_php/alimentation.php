<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  	
?>
       
			<div class="main">
            
	    <?php  //include "partial/maincontent/alimentation.php" ?>
         <div class="maincontent">
          
         <h1>Alimentation</h1>
         <p>Dans le but de r&eacute;pondre &agrave; leurs besoins nutritionnels, La Gribouille offre deux collations et un repas par jour aux enfants qui fr&eacute;quentent le service &eacute;ducatif. Comme dans le monde des Centres de la petite enfance, nous nous assurons d&rsquo;offrir des aliments sains qui respectent le Guide alimentaire canadien. Pour ce faire, nous travaillons en collaboration avec la caf&eacute;t&eacute;ria du coll&egrave;ge pour le repas du midi et les collations, qui sont choisies en fonction des portions recommand&eacute;es par le Guide alimentaire canadien. C&rsquo;est ainsi que nous nous rapprochons de la r&eacute;alit&eacute; v&eacute;cue en CPE.</p>
         <p>Nous vous pr&eacute;sentons Ga&eacute;tan, qui a &eacute;t&eacute; notre chef cuisinier de 1986 &agrave; 2009&nbsp;!</p>


         <p><img src="img/images/Gaetan.jpg" width="300" height="403" alt="Ga&eacute;tan, le cuisinier de La Gribouille" /></p>


         <p style="text-align:center;">&nbsp;</p>
            
              
          </div>
          </div>

                          			
<?php


?>