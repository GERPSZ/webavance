<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

  	require_once("partial/header2.php");
  	
?> 
   	 
     
            
			 <div class="main">
           
				<header class="codrops-header headerindex">
					<h1> La Gribouille <span> Bienvenue à La Gribouille </span></h1>
        </header>
                
             <div class="maincontent">  
             <p>  La Gribouille s'est dot&eacute; d'une <strong>triple mission</strong>&nbsp;:<br/>
                
             <br/>
             <ol>
               <li>offrir un milieu de garde pour  les parents qui &eacute;tudient ou travaillent au CVM;</li>
               <li>offrir un milieu de  vie éducatif pour les enfants;</li> 
               <li>être une ressource pédagogique, principalement pour l’enseignement des techniques humaines.</li>
            </ol>
            <p> La Gribouille offre des  services de garde de qualité et diversifiés, tout en adoptant des attitudes      pédagogiques préventives et constructives. De plus, La Gribouille s’engage à contribuer au  bien-être et au développement global et harmonieux des enfants qui la fréquentent.</p>
      
	  <div class="photoindex">
	  <p style="text-align:center;"><img src="img/images/gouache.jpg" alt="La gouache, quel plaisir!" width="400" height="267" longdesc="images/gouache.jpg" /></p>
	  <p style="text-align:center;">&nbsp;</p>
	  <p style="text-align:center;">&nbsp;</p>
      </div>
        </div><!-- .maincontent -->
			</div><!-- /main -->

        <div style="clear:both;"></div>     
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		
	</body>
</html>

<?php




?>