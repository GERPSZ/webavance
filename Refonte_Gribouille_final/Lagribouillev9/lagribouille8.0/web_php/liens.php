<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");


   
?>



            
			<div class="main">
            
		  <header class="codrops-header lienstitle">    
			<h1> Liens Utiles </h1>	
			</header>
                
           <div class="maincontent">
          
           <p>Ces liens vous sont propos&eacute;s &agrave; des fins de r&eacute;f&eacute;rence uniquement. Ils vous m&egrave;neront  sur des sites externes qui ne sont pas g&eacute;r&eacute;s par La Gribouille.<br />
<br /></p>
  <ul>
  <li><a href="http://www.cvm.qc.ca/activitesservices/servicesaide/radar/Pages/index.aspx" target="_blank" class="hyper"><strong>RADAR</strong></a> (service d&rsquo;aide aux &eacute;tudiants du CVM)</li>
  <li><a href="http://www.phac-aspc.gc.ca/" target="_blank" class="hyper"><strong>Sant&eacute;  Canada</strong></a> (rappel de mat&eacute;riel dangereux)</li>
  <li><a href="http://www.mfa.gouv.qc.ca/fr/Pages/index.aspx" target="_blank" class="hyper"><strong>Minist&egrave;re  de la Famille et des A&icirc;n&eacute;s</strong></a> (<em>Politique familiale du gouvernement du Qu&eacute;bec</em> et les services de garde &agrave;  l'enfance,&nbsp;donn&eacute;es sur les familles qu&eacute;b&eacute;coises, r&eacute;pertoire des services  de garde au Qu&eacute;bec, r&eacute;pertoire d'activit&eacute;s ext&eacute;rieures, etc.) </li>
  <li><a href="http://www.aveclenfant.com/index2.htm" target="_blank" class="hyper"><strong>Sylvie  Bourcier</strong></a> (intervenante en petite enfance,  consultante et formatrice; articles)</li>
  <li><a href="http://www.enfantsquebec.com/" target="_blank" class="hyper"><strong>Magazine enfants Qu&eacute;bec</strong></a> (&eacute;ducation,  sant&eacute;, psychologie, sorties, activit&eacute;s familiales) </li>
  <li><a href="http://www.aqaa.qc.ca/" target="_blank" class="hyper"><strong>Association qu&eacute;b&eacute;coise des allergies  alimentaires</strong></a></li>
  <li><a href="http://www.naitreetgrandir.net" target="_blank" class="hyper"><strong>www.naitreetgrandir.ne</strong>t</a> (la Fondation Lucie et Andr&eacute; Chagnon et PasseportSant&eacute;.net lancent un site Web sur le d&eacute;veloppement et la sant&eacute; des enfants pour aider les parents. Ce site qu&eacute;b&eacute;cois met &agrave; la disposition des parents des renseignements objectifs, pratiques, clairs, fiables et enti&egrave;rement valid&eacute;s par des experts.)</li>
  <li><a href="http://www.enfant-encyclopedie.com/fr-ca/accueil.html" target="_blank" class="hyper"><strong>L&rsquo;Encyclop&eacute;die sur le d&eacute;veloppement des jeunes enfants</strong></a> (publi&eacute;e sur Internet et accessible gratuitement, elle couvre des th&egrave;mes traitant du d&eacute;veloppement psychosocial de l&rsquo;enfant, de la conception &agrave; cinq ans, et pr&eacute;sente les connaissances scientifiques les plus r&eacute;centes. Les messages &agrave; retenir sur chacun des th&egrave;mes, pr&eacute;sent&eacute;s dans un format pratique, sont destin&eacute;s aux parents et aux intervenants.)</li>
  <li><a href="http://www.yoopa.ca/" title="Yoopa" target="_blank" class="hyper"><strong>Yoopa</strong></a> est un site destin&eacute; aux parents qui aborde une multitude de th&egrave;mes en lien avec les enfants (sant&eacute;, &eacute;ducation, psychologie, vie de famille, activit&eacute;s, etc.). Il offre de nombreuse r&eacute;f&eacute;rences et id&eacute;es inspirantes qui peuvent servir &agrave; tous ceux qui travaillent de pr&egrave;s ou de loin avec les enfants.</li>
  <li><a href="http://www.cccf-fcsge.ca/home_fr.html" title="Fédération canadienne des services de  garde à l’enfance" target="_blank" class="hyper"><strong>Fédération canadienne des services de  garde à l’enfance</strong></a>, c'est une référence  pour les intervenants en petite enfance. Ce site aborde plusieurs recherches faites par de grands spécialistes de l’enfance ainsi que plusieurs informations sur les milieux de garde. Il est également possible d’être membre et de recevoir des publications et des fiches d’informations diversifiées, ainsi qu’une revue scientifique au sujet de l’enfance. </li>
  
  <li><a href="http://www.chu-sainte-justine.org/Accueil/default.aspx" title="CHU Sainte-Justine" target="_blank" class="hyper"><strong>CHU Sainte-Justine</strong></a> contient des références  sur divers comportements et maladies chez l’enfant. Un site à découvrir pour les multitudes de ressources qui s’y trouve (conseils, soirée-parents,  conférences, publications etc.).</li>
  <li><a href="http://www.nospetitsmangeurs.org" target="_blank" class="hyper"><strong>Nospetitsmangeurs.org</strong></a><br />
    Voici un lien fort utile sur tout ce qui concerne l&rsquo;alimentation de vos enfants.  Id&eacute;es cuisines, trucs et conseils, comment cr&eacute;er un environnement alimentaire sain &agrave; vos petits et plus encore. Le site est sp&eacute;cialement con&ccedil;u pour les parents, les sp&eacute;cialistes en petite enfance, les responsables de milieux de garde et de cuisines. &Agrave; voir!<br />
  </li>
  </ul>
  <p style="text-align:center;">&nbsp;</p>
	  <p style="text-align:center;">&nbsp;</p>
            
            
            	
          </div>
          </div>
            
               
     
<?php

 


?>