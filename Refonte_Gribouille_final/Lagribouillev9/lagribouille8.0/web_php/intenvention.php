<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :   GPS                       *
 *                                         *
 *---------------------------------------- */

require_once("partial/header2.php");
  	
?>

          
			<div class="main">
            
			
                
           <div class="maincontent">
          
           <h1>Intervention &eacute;ducative</h1>
<p>L&rsquo;intervention &eacute;ducative est le processus par lequel le personnel &eacute;ducateur et les RSG (responsables de garde en milieu familial) agissent aupr&egrave;s de chacun des enfants de fa&ccedil;on &agrave; r&eacute;pondre le mieux possible &agrave; ses besoins. Elle comporte quatre &eacute;tapes.</p>
<h3>L&rsquo;observation</h3>
<p>Cette &eacute;tape, importante mais souvent n&eacute;glig&eacute;e, permet de conna&icirc;tre les go&ucirc;ts, les besoins et les capacit&eacute;s de chaque enfant. L&rsquo;information recueillie oriente les interventions, tout en alimentant les discussions avec les parents. Afin d&rsquo;en faciliter l&rsquo;analyse, les observations sont consign&eacute;es par &eacute;crit &agrave; l&rsquo;aide de diff&eacute;rents outils : fiche anecdotique, grille d&rsquo;observation, journal de bord, feuille de rythme (pour les poupons), etc.</p>
<h3>La planification et l&rsquo;organisation</h3>
<p>Cette &eacute;tape permet de pr&eacute;voir les activit&eacute;s et les interventions qui r&eacute;pondront le mieux aux besoins et aux go&ucirc;ts des enfants, d&rsquo;une mani&egrave;re &eacute;quilibr&eacute;e, de s&eacute;lectionner le mat&eacute;riel et de pr&eacute;parer l&rsquo;environnement physique de fa&ccedil;on &agrave; ce que les activit&eacute;s se d&eacute;roulent sans encombre. L&rsquo;&eacute;tablissement d&rsquo;un horaire quotidien donne des points de rep&egrave;re aux enfants et permet d&rsquo;assurer une bonne transition entre les activit&eacute;s. Cet horaire respecte le rythme de d&eacute;veloppement des enfants et demeure souple afin de laisser de la place pour les impr&eacute;vus.</p>
<h3>L&rsquo;intervention</h3>
<p>Au cours de cette &eacute;tape, le personnel &eacute;ducateur ou les RSG accompagnent les enfants dans leurs activit&eacute;s et interviennent au besoin pour les soutenir et les encourager. Ils ou elles enrichissent leurs jeux en proposant des variantes ou en y introduisant des &eacute;l&eacute;ments nouveaux de fa&ccedil;on &agrave; ce que les enfants se d&eacute;veloppent en allant du connu vers l&rsquo;inconnu.</p>
<h3>La r&eacute;flexion-r&eacute;troaction</h3>
<p>Cette &eacute;tape permet au personnel &eacute;ducateur ou aux RSG de se questionner sur leurs pratiques et de r&eacute;ajuster leurs interventions. Elle leur permet &eacute;galement d&rsquo;&eacute;valuer l&rsquo;ensemble des &eacute;l&eacute;ments qui ont &eacute;t&eacute; mis en place afin d&rsquo;assurer le d&eacute;veloppement global et harmonieux des enfants. Elle contribue enfin &agrave; la coh&eacute;rence dans les interventions de l&rsquo;&eacute;quipe &eacute;ducative et &agrave; l&rsquo;am&eacute;lioration de la qualit&eacute; des services. Il existe &eacute;galement diff&eacute;rents styles d&rsquo;intervention, c&rsquo;est-&agrave;-dire diff&eacute;rentes fa&ccedil;ons d&rsquo;intervenir aupr&egrave;s des enfants en milieu de garde. Certains sont plus autoritaires, d&rsquo;autres plus permissifs et d&rsquo;autres enfin plus d&eacute;mocratiques.</p>
<h3>L'intervention d&eacute;mocratique</h3>
<p>Elle favorise le libre choix de l&rsquo;enfant et l&rsquo;incite &agrave; participer aux d&eacute;cisions, dans la mesure de ses capacit&eacute;s et dans le respect de certaines r&egrave;gles de conduite et de s&eacute;curit&eacute;. L&rsquo;adulte soutient l&rsquo;enfant dans ses initiatives tout en respectant son rythme de d&eacute;veloppement. Il encourage l&rsquo;enfant &agrave; entretenir des relations avec ses pairs et &agrave; prendre sa place dans le groupe. En misant sur les champs d&rsquo;int&eacute;r&ecirc;t et les forces de chaque enfant, ce style d&rsquo;intervention favorise son autonomie et sa confiance en lui tout en lui offrant de belles occasions de socialiser.</p>
<p>Pour plus d'information, consultez le <a href="../images/TableauIntervDemocr.gif" target="_blank" class="hyper"><strong>tableau des styles d&rsquo;intervention</strong></a>.</p>
<p>Source&nbsp;: minist&egrave;re de la Famille et des A&icirc;n&eacute;s, <em>Accueillir  la petite enfance</em>, le programme &eacute;ducatif des services de garde du Qu&eacute;bec, mise  &agrave; jour de 2007.<br />
</p>
<p style="text-align:center;">&nbsp;</p>
            
            
            
            	
          </div>
          </div>
            
                
<?php




?>