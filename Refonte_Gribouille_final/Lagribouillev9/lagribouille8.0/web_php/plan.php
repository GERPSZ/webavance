<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  

?>

         
			<div class="main">
            
	     
			<header class="codrops-header">    
			<h1> Plan du site <span> Accueil </span></h1>	
			</header>
                
           <div class="maincontent">
                
            <h3><a href="index.php" class="hyper">Accueil</a><br />
           </h3>
            <h3><a href="nousjoindre.php" class="hyper">Nous joindre</a><br />
            </h3>
            <h3><a href="milieu.php" class="hyper">Milieu de garde</a><br />
           </h3>
            <p>&nbsp;&nbsp;&nbsp;<a href="services.php" class="hyper">Services offerts</a><br />
           </p>
           <p>&nbsp;&nbsp;&nbsp;<a href="horaire.php" class="hyper">Horaire quotidien</a><br />
           </p>
           <p>&nbsp;&nbsp;<a href="inscription.php" class="hyper">Inscription en ligne</a><br />
           </p>
           <p>&nbsp;&nbsp;&nbsp;<a href="regie.php" class="hyper">Régie interne</a><br />
           </p>
           <h3><a href="programme.php" class="hyper">Programme &eacute;ducatif</a><br />
           </h3>
           <p>&nbsp;&nbsp;<a href="objectifs.php" class="hyper">Objectifs</a><br />
          </p>
          <p>&nbsp;&nbsp;&nbsp;<a href="principes.php" class="hyper">Principes de base</a><br />
         </p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="developpement.php" class="hyper">D&eacute;veloppement de l'enfant</a><br />
</p>
<p>&nbsp;&nbsp;<a href="intervention.php" class="hyper">Intervention &eacute;ducative</a><br />
</p>
<p>&nbsp;&nbsp;<a href="structuration.php" class="hyper">Structuration des activit&eacute;s</a><br />
</p>
<h3><a href="labo.php" class="hyper">Laboratoire d'observation</a><br />
</h3>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="roleeducateurs.php" class="hyper">R&ocirc;le des &eacute;ducateurs</a><br />
</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="ethique.php" class="hyper">Éthique d'observation</a><br />
</p>
<h3><a href="equipe.php" class="hyper">&Eacute;quipe</a><br />
</h3>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="personnel.php" class="hyper">Personnel</a><br />
</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="alimentation.php" class="hyper">Alimentation</a><br />
</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="tee.php" class="hyper">&Eacute;tudiants en TEE</a><br />
</p>
<h3><a href="parents.php" class="hyper">Place des parents</a><br />
</h3>
<h3><a href="liens.php" class="hyper">Liens utiles</a></h3>
<h3><a href="gribverte.php" class="hyper">Gribouille verte</a></h3>
<p>&nbsp;</p>
<p></p>
			
			
          </div>
         
               
		<script src="../js/classie.js"></script>
		
        </div>
	</body>
</html>

<?php




?>