<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");

?>
        
			<div class="main">
            
		       
           <div class="maincontent">
          
           	<h1>&Eacute;thique d'observation</h1>
<p><strong>Ici &agrave; La Gribouille,  il y a des r&egrave;gles de fonctionnement et une &eacute;thique d'observation. Les  voici...</strong></p>
<p>La Gribouille est ouverte depuis avril 1987 et accueille des enfants de 24 mois &agrave; 5 ans. La Gribouille, avec sa salle d&rsquo;observation, est aussi une ressource p&eacute;dagogique pour les &eacute;tudiants, les professeurs, les  parents et les intervenants. Il est entendu que les r&egrave;gles qui suivent sont &eacute;nonc&eacute;es dans le but de nous permettre d&rsquo;assurer en tout temps le bien-&ecirc;tre des enfants. De plus, ces informations vous seront tr&egrave;s utiles dans la pr&eacute;paration de vos observations ou de vos animations.</p>
<h2>&Agrave; la salle d&rsquo;observation</h2>
<p><strong>S&rsquo;abstenir de fumer, de boire et de manger dans la salle d&rsquo;observation.<br />
  <br />
</strong></p>
<ul>
  <li>Les parents ont acc&egrave;s &agrave; la  salle d&rsquo;observation quand ils le d&eacute;sirent. Cependant, cette accessibilit&eacute; pourrait &ecirc;tre limit&eacute;e en fonction des besoins des cours et des travaux des &eacute;tudiants.</li>
  <li>Quand la salle d&rsquo;observation est utilis&eacute;e &agrave; des fins p&eacute;dagogiques, une priorit&eacute; est accord&eacute;e aux besoins d&rsquo;observation des professeurs des d&eacute;partements de Techniques d&rsquo;&eacute;ducation &agrave; l'enfance et de Techniques d&rsquo;&eacute;ducation sp&eacute;cialis&eacute;e.</li>
  <li>Veuillez inscrire vos besoins d&rsquo;observation en d&eacute;but de session sur la grille horaire r&eacute;serv&eacute;e &agrave; cet effet, sur la porte du local A8.56. Cela permet &agrave; l&rsquo;&eacute;ducateur de planifier sa programmation d&rsquo;ensemble, par exemple, pour r&eacute;server la piscine, le gymnase, etc. Certaines p&eacute;riodes d'observations ponctuelles peuvent &ecirc;tre r&eacute;serv&eacute;es par la suite, au  moins une semaine &agrave; l&rsquo;avance, aux p&eacute;riodes libres de la grille, en avisant le  personnel &eacute;ducateur au poste 2225.</li>
  <li>Il est possible de prendre des ententes avec le personnel &eacute;ducateur pour des besoins d&rsquo;observation  particuliers (par ex., l&rsquo;apprentissage d&rsquo;une chanson). Toutefois, les besoins et les int&eacute;r&ecirc;ts des enfants &eacute;tant une priorit&eacute;, l&rsquo;&eacute;ducatrice a la libert&eacute;  d'accepter ou de refuser une demande pr&eacute;cise.</li>
  <li>Dans le but de maintenir un  esprit d&rsquo;&eacute;change entre le personnel &eacute;ducateur et les utilisateurs de la salle d&rsquo;observation, veillez &agrave; informer les &eacute;ducateurs des motifs de l&rsquo;observation et de ses r&eacute;sultats.</li>
  <li>Une cl&eacute; est disponible pour  vous donner acc&egrave;s &agrave; la salle d&rsquo;observation. Vous devez l&rsquo;emprunter &agrave; la Mat&eacute;riath&egrave;que (A9.60). Assurez-vous de rapporter la cl&eacute; et de laisser le local propre et verrouill&eacute;. Vous pouvez refuser l&rsquo;acc&egrave;s aux personnes inconnues qui ne veulent pas s&rsquo;identifier.</li>
</ul>
<h3 class="Style1"><strong>TR&Egrave;S IMPORTANT!<br />
</strong></h3>
<ul>
  <li>Lors d&rsquo;observations non formelles, le silence est souhait&eacute; dans la salle d&rsquo;observation.</li>
  <li>La plus grande discr&eacute;tion  sur chaque enfant de La Gribouille est exig&eacute;e. &Eacute;vitez de formuler des  commentaires sur les enfants autant en pr&eacute;sence des parents que dans les corridors, m&ecirc;me si c&rsquo;est positif ou rigolo.</li>
  <li>Si vous devez discuter de  vos observations sur les enfants pour les besoins d&rsquo;un cours, veuillez parler en termes de comportements en ayant soin d&rsquo;&eacute;viter les jugements. Toute personne  qui r&eacute;dige un rapport d&rsquo;observation doit &eacute;viter d&rsquo;identifier l&rsquo;enfant pour  assurer la confidentialit&eacute;.</li>
</ul>
<h2>2. &Agrave; La Gribouille</h2>
<ul>
  <li>La Gribouille, en tant que ressource p&eacute;dagogique, permet la  r&eacute;alisation de diff&eacute;rents projets. Vous devez n&eacute;anmoins prendre en  consid&eacute;ration l&rsquo;horaire des enfants. Le local et le mat&eacute;riel de La  Gribouille peuvent aussi &ecirc;tre utilis&eacute;s en dehors des heures de  fr&eacute;quentation des enfants, en pr&eacute;sence du personnel &eacute;ducateur ou d'un  enseignant.</li>
  <li>Afin de tenir des statistiques de fr&eacute;quentation, veuillez inscrire toute utilisation du local. Pour le bien-&ecirc;tre des enfants, il est impossible d'animer une  activit&eacute; &agrave; La Gribouille.</li>
</ul>
<h2>3. Demande et r&eacute;alisation d&rsquo;un  projet</h2>

  La  Gribouille peut &ecirc;tre utilis&eacute;e  uniquement &agrave; des fins p&eacute;dagogiques. Toute demande doit &ecirc;tre soumise au moins une semaine &agrave; l&rsquo;avance &agrave; l&rsquo;aide d'un formulaire disponible sur demande au personnel &eacute;ducateur. Chaque demande sera consid&eacute;r&eacute;e par le personnel &eacute;ducateur responsable de La Gribouille, qui y  r&eacute;pondra dans un bref d&eacute;lai. Si le projet implique la participation des enfants, il faudra obtenir l&rsquo;autorisation &eacute;crite des parents. Vous devez  pr&eacute;ciser l&rsquo;utilisation future de tout document audiovisuel.
  <strong>Avant la r&eacute;alisation</strong> du projet, prendre connaissance des r&egrave;gles de fonctionnement.
  <strong>Apr&egrave;s la r&eacute;alisation</strong> du projet, faire part au comit&eacute; des r&eacute;sultats obtenus.

            
            
            	
          </div>
          </div>
            
                
<?php

 
?>