<?php
/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                    *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  	 
?>
       
			<div class="main">
            
      <header class="codrops-header equipetitle">    
		 	<h1>L'equipe de la gribouille</h1>	
			</header>
                
      <div class="maincontent">    
           	
						<ul>
							<li><a href="personnel.php"><i class="fa fa-fw fa-thumbs-up"></i>Personnel</a></li>
							<li><a href="alimentation.php"><i class="fa fa-fw fa-spoon"></i>Alimentation</a></li>
							<li><a href="tee.php"><i class="fa fa-fw fa-user"></i>Étudiants TEE</a></li>				
	      	 	</ul>
				
			         
      <p>Tous les membres du  personnel &eacute;ducateur de  La Gribouille  poss&egrave;dent un DEC en Techniques d&rsquo;&eacute;ducation &agrave; l&rsquo;enfance. De plus, ils respectent  et appliquent le programme &eacute;ducatif du minist&egrave;re de la Famille, tout comme le personnel &eacute;ducateur  travaillant dans les CPE.</p>
      <p>Ainsi, les employ&eacute;s de La Gribouille offrent des interventions appropri&eacute;es, une  organisation des observations en collaboration avec les enseignants de TEE et  de TES, une structuration des lieux, un choix d&rsquo;&eacute;quipements appropri&eacute;s, une  organisation de l&rsquo;horaire et des activit&eacute;s de tous les moments de vie.</p>
      <p>Ces  mod&egrave;les d&rsquo;&eacute;ducateurs sont n&eacute;cessaires au soutien &agrave; l&rsquo;enseignement afin d&rsquo;&eacute;tablir  les liens entre la th&eacute;orie et la pratique, et ce, en p&eacute;n&eacute;trant  dans la r&eacute;alit&eacute; d&rsquo;un groupe d&rsquo;enfants et en observant comment l&rsquo;&eacute;ducateur intervient,  anime, d&eacute;veloppe l&rsquo;estime de soi et l&rsquo;autonomie des enfants afin que ceux-ci  s&rsquo;&eacute;panouissent pleinement &agrave; travers le jeu.</p>
      </div>        
           	
                
      </div>
            
              				

<?php

   

?>