<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :   GPS                       *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  

?>

            
			<div class="main">
            
		<div class="maincontent">
          
 <h1>R&ocirc;le des &eacute;ducateurs</h1>
    <p>Les employ&eacute;s de La Gribouille offrent des interventions appropri&eacute;es, une organisation des observations en collaboration avec les enseignants des Techniques d'&eacute;ducation &agrave; l'enfance (TEE) et des Techniques d'&eacute;ducation sp&eacute;cialis&eacute;e (TES), une structuration des lieux, un choix d&rsquo;&eacute;quipements appropri&eacute;s, une organisation de l&rsquo;horaire et des activit&eacute;s pour tous les moments de vie.</p>
    <p>Ces mod&egrave;les d&rsquo;&eacute;ducateurs sont n&eacute;cessaires au soutien &agrave; l&rsquo;enseignement afin d&rsquo;&eacute;tablir les liens entre la th&eacute;orie et la pratique, et ce, en p&eacute;n&eacute;trant dans la r&eacute;alit&eacute; d&rsquo;un groupe d&rsquo;enfants et en observant comment l&rsquo;&eacute;ducateur intervient, anime, d&eacute;veloppe l&rsquo;estime de soi et l&rsquo;autonomie des enfants afin que ceux-ci s&rsquo;&eacute;panouissent pleinement &agrave; travers le jeu.</p>
    <p style="text-align:center;">&nbsp;</p>
	  <p style="text-align:center;">&nbsp;</p>

            
            
            	
    </div>
    </div>
            

        
		<script src="js/classie.js"></script>
        
  
	</body>
</html>

<?php




?>