<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :   GPS                       *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  	
?>
       
           
			<div class="main">
       
            <header class="codrops-header titleprogramme">    
			<h1> Programme éducatif </h1>	
			</header>
                
      <div class="maincontent">
      <div class="photoedu">      
      <a href="http://www.mfa.gouv.qc.ca/fr/publication/Documents/programme_educatif.pdf"><img src="img/images/programme_educatif.jpg"  width="500" alt="Programme éducatif des services de garde du Québec"/></a>
      </div>
      	
						
						<ul>
							<li><a href="objectifs.php"><i class="fa fa-fw fa-tags"></i>Objectifs</a></li>
							<li><a href="principes.php"><i class="fa fa-fw fa-tasks"></i>Principes de base</a></li>
							<li><a href="developpement.php"><i class="fa fa-fw fa-child"></i>Développement de l'enfant</a></li>
							<li><a href="intenvention.php"><i class="fa fa-fw fa-laptop"></i>Intervention éducative</a></li>
                            <li><a href="structuration.php"><i class="fa fa-fw fa-cubes"></i>Structuration des Activités</a></li>		
					 </ul>
				
				
            
	    <p>Le fonctionnement de La Gribouille est bas&eacute; sur  les principes du programme  &eacute;ducatif <a href="http://www.mfa.gouv.qc.ca/fr/publication/Documents/programme_educatif.pdf" target="_blank" class="hyper"><strong>Accueillir la petite enfance</strong></a> (PDF) agr&eacute;&eacute;  par le minist&egrave;re de la Famille  en lien avec le programme Techniques d&rsquo;&eacute;ducation &agrave; l&rsquo;enfance.</p>
      <p>Le programme &eacute;ducatif de La Gribouille met l&rsquo;accent sur le jeu, car celui-ci constitue pour l&rsquo;enfant  l&rsquo;instrument par excellence pour explorer l&rsquo;univers, le comprendre et le  ma&icirc;triser. Ainsi, le d&eacute;veloppement global de l&rsquo;enfant se poursuit &agrave;  travers une s&eacute;rie d&rsquo;actions qu&rsquo;on appelle des &laquo;&nbsp;<strong><a href="Experiences-prescolaire.pdf" target="_blank" class="hyper">exp&eacute;riences&nbsp;cl&eacute;s</a></strong>&nbsp;&raquo; (PDF). Il vit ces exp&eacute;riences &agrave; tout moment de  la journ&eacute;e,  il les r&eacute;p&egrave;te jour apr&egrave;s jour et les transpose dans diverses  situations.</p>
      <p>C&rsquo;est ainsi qu&rsquo;&agrave; La Gribouille nous pr&ocirc;nons l&rsquo;apprentissage actif et l&rsquo;approche d&eacute;mocratique. Ces m&eacute;thodes nous permettent d&rsquo;appliquer le programme &eacute;ducatif des Centres de la petite enfance et de servir de vitrine sur le monde des CPE.</p>
      <p style="text-align:center;">&nbsp;</p>
	    <p style="text-align:center;">&nbsp;</p>
                 
      </div>
                 
                 

        
		<script src="../js/classie.js"></script>
		
        </div>
	</body>
</html>

<?php




?>