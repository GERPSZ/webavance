<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  
?>

            
			<div class="main">
                            
      <div class="maincontent">
          
           <h1>Structuration des activit&eacute;s</h1>
<p>La capacit&eacute; des services de garde d&rsquo;atteindre leurs objectifs d&eacute;pend en grande partie de l&rsquo;organisation des activit&eacute;s qui s&rsquo;y d&eacute;roulent quotidiennement. En offrant un programme d&rsquo;activit&eacute;s qui respecte les principes de base du programme &eacute;ducatif et qui favorise le d&eacute;veloppement des enfants dans chacune de ses dimensions, les milieux de garde contribuent &agrave; leur d&eacute;veloppement harmonieux.</p>
<h3>Les activit&eacute;s de routine et de transition</h3>
<p>Loin d&rsquo;&ecirc;tre anodines, les activit&eacute;s de routine et de transition sont &agrave; la base de la planification de l&rsquo;horaire quotidien du service de garde. Elles comprennent l&rsquo;accueil et le d&eacute;part, les repas et les collations, les soins d&rsquo;hygi&egrave;ne, la sieste ou la d&eacute;tente et le rangement. Les activit&eacute;s de routine des b&eacute;b&eacute;s peuvent m&ecirc;me occuper jusqu&rsquo;&agrave; 80&nbsp;% de l&rsquo;horaire quotidien. Elles sont autant d&rsquo;occasions de stimuler toutes les dimensions du d&eacute;veloppement de l&rsquo;enfant et de lui faire acqu&eacute;rir de saines habitudes de vie, particuli&egrave;rement en ce qui concerne l&rsquo;alimentation et l&rsquo;hygi&egrave;ne. Il est d&rsquo;ailleurs important de demander la collaboration des parents afin qu&rsquo;ils encouragent l&rsquo;enfant &agrave; maintenir ces habitudes (se laver les mains avant un repas, par exemple) &agrave; la maison.</p>
<h3>Les p&eacute;riodes de jeu</h3>
<p>Toute activit&eacute; peut devenir un jeu pour l&rsquo;enfant. De caract&egrave;re spontan&eacute;, le jeu se caract&eacute;rise avant tout par le plaisir qu&rsquo;il procure, et c&rsquo;est pourquoi l&rsquo;enfant s&rsquo;y investit avec beaucoup d&rsquo;int&eacute;r&ecirc;t et y consacre beaucoup de temps. C&rsquo;est son mode d&rsquo;exploration du monde. Quel que soit son type ou sa forme, le jeu pr&eacute;sente des d&eacute;fis &agrave; relever, des probl&egrave;mes &agrave; r&eacute;soudre et des r&egrave;gles &agrave; respecter. C&rsquo;est parce qu&rsquo;il s&rsquo;y investit &agrave; fond que l&rsquo;enfant fait, par l&rsquo;interm&eacute;diaire du jeu, des apprentissages qui touchent toutes les facettes de son d&eacute;veloppement. Qu&rsquo;il soit individuel ou collectif, le jeu en service de garde stimule l&rsquo;enfant, principalement<br />
  &agrave; cause du mat&eacute;riel pr&eacute;sent&eacute;. Il se d&eacute;roule autant &agrave; l&rsquo;ext&eacute;rieur du service de garde qu&rsquo;&agrave; l&rsquo;int&eacute;rieur. La cour ext&eacute;rieure, le parc ou tout autre endroit propice offre aux enfants de nombreuses occasions de d&eacute;couvertes et d&rsquo;apprentissage par le jeu.</p>
<p>Tout enfant a besoin de participer &agrave; des activit&eacute;s qui correspondent &agrave; son niveau de d&eacute;veloppement et &agrave; ses capacit&eacute;s. Le choix de l&rsquo;activit&eacute; propos&eacute;e se fait donc selon des th&egrave;mes qui correspondent &agrave; ses go&ucirc;ts. Lorsqu&rsquo;il a acc&egrave;s &agrave; des exp&eacute;riences d&rsquo;un niveau ad&eacute;quat pour lui, l&rsquo;enfant se sent motiv&eacute; et capable de r&eacute;ussir. Dans certains cas, l&rsquo;adulte lui permettra de r&eacute;p&eacute;ter une action nouvellement apprise pour en faciliter la consolidation; dans d&rsquo;autres, il accro&icirc;tra le niveau de difficult&eacute; d&rsquo;une activit&eacute; par l&rsquo;apport de nouveau mat&eacute;riel et offrira ainsi un d&eacute;fi additionnel &agrave; l&rsquo;enfant, quitte &agrave; le soutenir si n&eacute;cessaire.</p>
<h3>La planification</h3>
<p>En planifiant la journ&eacute;e, l&rsquo;adulte responsable d&rsquo;un groupe d&rsquo;enfants tient compte de sa connaissance des enfants pour mettre &agrave; leur disposition un mat&eacute;riel de jeu stimulant, vari&eacute; et adapt&eacute; &agrave; leur niveau de d&eacute;veloppement, et ce, tout en  leur pr&eacute;sentant des activit&eacute;s qui touchent les objectifs de d&eacute;veloppement global. Apr&egrave;s avoir observ&eacute; les enfants de son groupe, il peut, par exemple, d&eacute;cider de leur offrir un mat&eacute;riel particulier, ou encore de leur faire vivre de nouvelles exp&eacute;riences. En suscitant leur curiosit&eacute;, il leur offre la possibilit&eacute; de diversifier leurs actions et d&rsquo;acqu&eacute;rir de nouvelles habilet&eacute;s. L&rsquo;adulte tient &eacute;galement compte des caract&eacute;ristiques li&eacute;es au sexe des enfants dont il a la responsabilit&eacute; (par exemple, les  besoins moteurs plus grands des gar&ccedil;ons et l'int&eacute;r&ecirc;t plus marqu&eacute; des filles pour des activit&eacute;s verbales).</p>
<p style="text-align:center;">Source&nbsp;: minist&egrave;re de la Famille et des A&icirc;n&eacute;s, <em>Accueillir  la petite enfance</em>, le programme &eacute;ducatif des services de garde du Qu&eacute;bec, mise  &agrave; jour de 2007.</p>
<p style="text-align:center;">&nbsp;</p>
	  <p style="text-align:center;">&nbsp;</p>
            
            
            	
      </div>
      </div>
            
                		
        
		<script src="../js/classie.js"></script>
        
	       
  	</body>
    </html>

<?php




?>