<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par : GPS                         *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  	

?>

          
           
			<div class="main">
            
			
                
           <div class="maincontent">
          
         <h1>Les principes de base du programme</h1>
<p>Des objectifs  d&eacute;coulent cinq principes &agrave; la base du programme &eacute;ducatif. Les quatre premiers sont li&eacute;s &agrave; l&rsquo;enfant, tandis que le dernier porte sur la relation tripartite entre l&rsquo;enfant, ses parents et l&rsquo;adulte qui en est responsable au service de garde. Ils s&rsquo;appliquent tous au quotidien dans les diff&eacute;rents milieux de garde &eacute;ducatifs.</p>
<h3>1- Chaque enfant est unique</h3>
<p>En d&eacute;veloppant une connaissance approfondie de chaque enfant, l&rsquo;adulte qui en est responsable est en mesure de reconna&icirc;tre et de respecter les particularit&eacute;s de chacun, son rythme de d&eacute;veloppement, ses besoins et ses champs d&rsquo;int&eacute;r&ecirc;t.</p>
<h3>2- L&rsquo;enfant est le premier agent de son d&eacute;veloppement</h3>
<p>Un enfant apprend d&rsquo;abord spontan&eacute;ment, en exp&eacute;rimentant, en observant, en imitant et en parlant avec les autres, gr&acirc;ce &agrave; sa propre motivation et &agrave; ses aptitudes naturelles. L&rsquo;adulte guide et soutient cette d&eacute;marche qui conduit &agrave; l&rsquo;autonomie.</p>
<h3>3- Le d&eacute;veloppement de l&rsquo;enfant est un processus global et int&eacute;gr&eacute;</h3>
<p>L&rsquo;enfant se d&eacute;veloppe dans toutes ses dimensions &ndash; affective, physique et motrice, sociale et morale, cognitive et langagi&egrave;re &ndash; et celles-ci agissent &agrave; des degr&eacute;s divers dans le cadre de ses apprentissages. Les interventions de l&rsquo;adulte, les am&eacute;nagements et les activit&eacute;s propos&eacute;es dans les services de garde sollicitent de multiples fa&ccedil;ons l&rsquo;ensemble de ces dimensions.</p>
<h3>4- L&rsquo;enfant apprend par le jeu</h3>
<p>Essentiellement le produit d&rsquo;une motivation int&eacute;rieure, le jeu constitue pour l&rsquo;enfant le moyen par excellence d&rsquo;explorer le monde et d&rsquo;exp&eacute;rimenter. Les diff&eacute;rents types de jeux auxquels il joue &ndash; solitaire ou coop&eacute;ratif, moteur, symbolique, etc. &ndash; sollicitent, chacun &agrave; leur mani&egrave;re, toutes les dimensions de sa personne.</p>
<h3>5- La collaboration entre le personnel &eacute;ducateur ou les RSG et les parents est essentielle au d&eacute;veloppement harmonieux de l&rsquo;enfant</h3>
<p>Il est important qu&rsquo;une bonne entente et un lien de confiance existent entre le personnel &eacute;ducateur ou les RSG et les parents. Cela rassure l&rsquo;enfant et favorise la cr&eacute;ation d&rsquo;un lien affectif privil&eacute;gi&eacute; entre lui et le ou les adultes qui en prennent soin au service de garde.</p>
<p></p>
<p style="text-align:center;">Source&nbsp;: minist&egrave;re de la Famille et des A&icirc;n&eacute;s, <em>Accueillir  la petite enfance</em>, le programme &eacute;ducatif des services de garde du Qu&eacute;bec, mise  &agrave; jour de 2007.</p>
            
            
            
            	
          </div>
          </div>
            
             
		<script src="../js/classie.js"></script>
       
           
	</body>
</html>

<?php




?>