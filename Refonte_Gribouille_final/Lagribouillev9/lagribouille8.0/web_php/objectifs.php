<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                       *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
 
?>
           
           
			<div class="main">
            
			
                
           <div class="maincontent">
          
<h1>Les objectifs des services de garde</h1>
<p>Au Qu&eacute;bec, les services de garde &eacute;ducatifs ont une triple mission :</p>
<ol>
  <li>voir au bien-&ecirc;tre, &agrave; la sant&eacute; et &agrave; la s&eacute;curit&eacute; des enfants qui leur sont confi&eacute;s;</li>
  <li>leur offrir un milieu de vie propre &agrave; stimuler leur d&eacute;veloppement sur tous les plans, de leur naissance &agrave; leur entr&eacute;e &agrave; l&rsquo;&eacute;cole;</li>
  <li>pr&eacute;venir l&rsquo;apparition ult&eacute;rieure de difficult&eacute;s d&rsquo;apprentissage, de comportement ou d&rsquo;insertion sociale. </li>
</ol>
<p>Ainsi, qu&rsquo;ils soient offerts en CPE, en garderie ou en milieu familial, tous les services de garde doivent  appliquer un programme &eacute;ducatif comportant des activit&eacute;s qui ont pour buts :</p>
<ul>
  <li>de favoriser le d&eacute;veloppement global de l&rsquo;enfant en lui permettant de d&eacute;velopper toutes les dimensions de sa personne, notamment sur le plan affectif, social, moral, cognitif, langagier, physique et moteur;</li>
  <li>d&rsquo;amener progressivement l&rsquo;enfant &agrave; s&rsquo;adapter &agrave; la vie en collectivit&eacute; et de s&rsquo;y int&eacute;grer harmonieusement. </li>
</ul>
<p>Le programme &eacute;ducatif comprend &eacute;galement des services de promotion et de pr&eacute;vention visant &agrave; donner &agrave; l&rsquo;enfant un environnement favorable au d&eacute;veloppement de saines habitudes de vie, de saines habitudes alimentaires et de comportements qui influencent de mani&egrave;re positive sa sant&eacute; et son bien-&ecirc;tre.</p>
<h3>Les objectifs des services de garde &eacute;ducatifs :</h3>
<ol>
  <li>Accueillir les enfants et r&eacute;pondre &agrave; leurs besoins;</li>
  <li>Assurer le bien-&ecirc;tre, la sant&eacute; et la s&eacute;curit&eacute; des enfants;</li>
  <li>Favoriser l&rsquo;&eacute;galit&eacute; des chances;</li>
  <li>Contribuer &agrave; la socialisation des enfants;</li>
  <li>Apporter un appui aux parents;</li>
  <li>Faciliter l&rsquo;entr&eacute;e de l&rsquo;enfant &agrave; l&rsquo;&eacute;cole.</li>
</ol>
<h3>Les objectifs du programme &eacute;ducatif&nbsp;:</h3>
<ol>
  <li>Assurer aux enfants des services de qualit&eacute;;</li>
  <li>Servir d&rsquo;outil de r&eacute;f&eacute;rence &agrave; toute personne travaillant dans le milieu des services de garde;</li>
  <li>Promouvoir la coh&eacute;rence entre les milieux de garde;</li>
  <li>Favoriser l&rsquo;arrimage de l&rsquo;ensemble des interventions faites aupr&egrave;s de la petite enfance et des familles ayant de jeunes enfants.</li>
</ol>
<p style="text-align:center;">Source&nbsp;: minist&egrave;re de la Famille et des A&icirc;n&eacute;s, <em>Accueillir  la petite enfance</em>, le programme &eacute;ducatif des services de garde du Qu&eacute;bec, mise  &agrave; jour de 2007.</p>

            
            
            
            	
          </div>
          </div>
            
              
        
		<script src="js/classie.js"></script>
        
		
           
	</body>
</html>
<?php




?>