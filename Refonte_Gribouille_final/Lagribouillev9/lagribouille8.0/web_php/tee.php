<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  	

?>


            
			<div class="main">
            
	        
           <div class="maincontent">
        <h1>&Eacute;tudiants en TEE</h1>
<p>Nous ne pourrions pas offrir un service de garde &eacute;ducatif de qualit&eacute; sans le travail en &eacute;quipe. Pour ce faire, nous incluons dans notre &eacute;quipe de travail une ou deux &eacute;tudiantes qui nous offrent leur soutien ou qui nous remplacent afin de faire diff&eacute;rentes rencontres soit en &eacute;quipe, en classe ou avec les enseignants. De cette fa&ccedil;on, nous sommes en mesure d&rsquo;&eacute;changer et d&rsquo;assurer une constance et une coh&eacute;rence aupr&egrave;s des enfants et, par le fait m&ecirc;me, nous offrons la chance aux futures &eacute;ducatrices d&rsquo;avoir une exp&eacute;rience pratique tout au long de leur formation. </p>
<p>Merci aux  pr&eacute;cieuses &eacute;tudiantes qui sont pass&eacute;es &agrave; La Gribouille !</p>
<p><img src="img/images/Audreanne.jpg" alt="Audr&eacute;anne, &eacute;tudiante en TEE" width="182" height="330" /><img src="img/images/etudianteTEE2.jpg" alt="Vanessa, &eacute;tudiante en TEE" width="180" height="330" /></p>
<p>Audr&eacute;anne et Vanessa, &eacute;tudiantes en TEE. </p>
<p style="text-align:center;">&nbsp;</p>
<p style="text-align:center;">&nbsp;</p>
<p style="text-align:center;">&nbsp;</p>
<p style="text-align:center;">&nbsp;</p>
<p style="text-align:center;">&nbsp;</p>
<p style="text-align:center;">&nbsp;</p>
<p style="text-align:center;">&nbsp;</p>
            
            
            	
          </div>
          </div>
            
              
        
		<script src="../js/classie.js"></script>
       
        
	</body>
</html>

<?php




?>