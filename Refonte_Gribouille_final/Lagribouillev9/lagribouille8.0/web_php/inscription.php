<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :  GPS                        *
 *                                         *
 *---------------------------------------- */

require_once("partial/header2.php");
  	
?>
      
			<div class="main">
            
			
                
           <div class="maincontent">
          
       <h1>Inscription en ligne</h1>
<p>Pour s’inscrire sur la liste  d’attente, il suffit de remplir le <a href="ListeAttenteGribouille.pdf" target="_blank" class="hyper"><strong>formulaire en ligne</strong></a> (PDF). Un responsable  communiquera avec vous le moment venu. Les appels se font surtout au mois de  mai pour la session qui suit &agrave; l’automne.</p>
<p> La  priorité du service est accordée aux parents étudiant au cégep du Vieux  Montréal, puis ensuite aux parents travaillant au CVM. S’il reste de la place,  les parents du quartier peuvent y inscrire leurs enfants.</p>
<h2>  Nos coordonnées</h2>
<p>  Cégep  du Vieux Montréal<br />
  255, rue Ontario Est, local A8.57<br />
  Casier 505
  <br />
  Montréal (Québec)  H2X 1X6</p>
<p>  Téléphone&nbsp;:  514 982-3437, poste 2225<br />
  Courriel&nbsp;: <a href="mailto:gribouille@cvm.qc.ca" class="hyper">gribouille@cvm.qc.ca</a></p>
<p>&nbsp;</p>
<p style="text-align:center;">&nbsp;</p>s
            	
          </div>
          </div>
            


<?php




?>