<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par :   GPS                       *
 *                                         *
 *---------------------------------------- */

require_once("partial/header2.php");


?>

        
			<div class="main">
            
			
                
        <div class="maincontent ">
        

        <h1>Horaire quotidien<br />
	      <br/>
	      </h1>


      <div id="horairenfants">
       <table width="90%" align="center" border="2" bordercolor="#1D4483" cellpadding="2px" class="horaire">
        <tr>
          <td width="15%" class="horaire"><p class="Style9">7h45 ou 8h00</p></td>
          <td width="49%" class="horaire"><p class="Style9">Accueil</p></td>
          <td width="36%" class="horaire"><p class="Style9">P&eacute;riode de jeux libres</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">9h00</p></td>
          <td class="horaire"><p class="Style9">Transition</p></td>
          <td class="horaire"><p class="Style9">Lavage des mains</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">9h10</p></td>
          <td class="horaire"><p class="Style9">Collation</p></td>
          <td class="horaire"><p class="Style9">Causerie et &eacute;changes</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">9h30</p></td>
          <td class="horaire"><p class="Style9">Transition</p></td>
          <td class="horaire"><p class="Style9">Toilette<br />  
              Changement des couches</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">10h00</p></td>
          <td colspan="2" class="horaire"><p class="Style9">Planification - ateliers libres - rangement - r&eacute;flexion &agrave; 11h00  ou p&eacute;riode en groupe d&rsquo;appartenance ou jeux ext&eacute;rieurs</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">11h00</p></td>
          <td class="horaire"><p class="Style9">Pr&eacute;paration des matelas pour la sieste, transition </p></td>
          <td class="horaire"><p class="Style9">Lavage des mains</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">11h30</p></td>
          <td class="horaire"><p class="Style9">D&icirc;ner&nbsp;</p></td>
          <td class="horaire"><p class="Style9">Causerie et &eacute;changes</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">12h00</p></td>
          <td class="horaire"><p>Transition<br />
          et jeux libres</p></td>
          <td class="horaire"><p>Toilette<br />
            Lavage des mains<br />
            Brossage des dents</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">12h45</p></td>
          <td colspan="2" class="horaire"><p class="Style9">Activit&eacute; de d&eacute;tente  (l&rsquo;heure du conte, chansons, etc.)</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">13h00</p></td>
          <td colspan="2" class="horaire"><p class="Style9">Sieste</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">14h15</p></td>
          <td colspan="2" class="horaire"><p class="Style9">Jeux de tables pour les enfants qui sont r&eacute;veill&eacute;s</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">15h00</p></td>
          <td class="horaire"><p class="Style9">Lever de la sieste<br />
            Transition<br />
          </p></td>
          <td class="horaire"><p class="Style9">Toilette<br />
          Lavage  des mains</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">15h20</p></td>
          <td class="horaire"><p class="Style9">Collation</p></td>
          <td class="horaire"><p class="Style9">Causerie et &eacute;changes</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">16h00</p></td>
          <td colspan="2" class="horaire"><p class="Style9">Jeux  libres ou activit&eacute;s en groupe d&rsquo;appartenance</p></td>
        </tr>
        <tr>
          <td class="horaire"><p class="Style9">17h30<br />
          18h00 </p></td>
          <td colspan="2" class="horaire"><p class="Style9">Fermeture</p></td>
        </tr>
      </table>
	  <p style="text-align:center;">&nbsp;</p>
	  <p style="text-align:center;">&nbsp;</p>
            
            	
          </div>
          </div>
            
           
                
			</div><!-- /main -->
                 
		
        
		<script src="../js/classie.js"></script>
        	
      
	</body>
</html>

<?php


  

?>