<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par : GPS                         *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");


?>



            
			<div class="main">
            
	      <header class="codrops-header">    
			<h1> Nous joindre </h1>	
			</header>
                
      <div class="maincontent">
          
       <p><strong>La Gribouille</strong><br />
        C&eacute;gep du Vieux Montr&eacute;al<br />
        255, rue Ontario Est, local A8.57<br />
        Montr&eacute;al (Qu&eacute;bec) H2X 1X6</p>
       <p>T&eacute;l&eacute;phone : 514 982-3437, poste 2225<br />
        Courriel : <a href="mailto:gribouille@cvm.qc.ca" class="hyper">gribouille@cvm.qc.ca</a></p>
        <p>La Gribouille est situ&eacute;e au local A8.57.<br />
        La salle d'observation se trouve au local A8.56.</p>
       <h3>Informations g&eacute;n&eacute;rales<br />
       <br />
       </h3>
       
         <ul>
         <li>La Gribouille dessert prioritairement les parents &eacute;tudiant au c&eacute;gep du Vieux Montr&eacute;al, ensuite les parents travaillant au CVM et finalement les parents du quartier.</li>
         <li>Les frais de garde sont les m&ecirc;mes que ceux des centres de la petite enfance, soit de 7 $ par jour. </li>
         <li>La Gribouille ouvre ses portes du lundi au vendredi, de 8 h &agrave; 18 h, et ce, en fonction des jours de classe pr&eacute;vus au calendrier scolaire de la session d&rsquo;automne et d&rsquo;hiver. </li>
         </ul>
        
          <p style="text-align:center;">&nbsp;</p>
	        <p style="text-align:center;">&nbsp;</p>
	        <p style="text-align:center;">&nbsp;</p>
            
                	
          </div>
          </div>
            
              

			    
		<script src="../js/classie.js"></script>
       
        
	</body>
</html>
<?php




?>