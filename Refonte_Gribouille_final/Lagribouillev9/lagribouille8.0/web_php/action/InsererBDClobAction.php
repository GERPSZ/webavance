<?php 
	require_once("action/CommonAction.php");
	require_once("action/Dao/UserDAO.php");

	
	class InsererBDClobAction extends CommonAction
	{
       public $mauvaiseLogin = false;
       private static $PAGE_NAME = "Connexion";

      public function __construct()
    {
      parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGE_NAME);
    }


       protected function executeAction()
       {
          
             $texteclob =!empty($_POST["editeurpages"]) ? $_POST["editeurpages"]:'';
             $page =!empty($_POST["page"]) ? $_POST["page"]:'';

            
             UserDAO::InsererClob($page,$texteclob);
            
       }


	}
	
?>