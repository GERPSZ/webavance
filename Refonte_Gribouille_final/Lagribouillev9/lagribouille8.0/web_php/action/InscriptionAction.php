<?php 
	require_once("action/CommonAction.php");
	require_once("action/DAO/UserDAO.php");


	class InscriptionAction extends CommonAction
	 {
        private static $PAGENAME = "Inscription";

		public function __construct()
		{
			parent::__construct(CommonAction::$VISIBILITY_ADMIN,self::$PAGENAME);
		}

		protected function executeAction()

		{

             $this->retour['id']=!empty($_POST["iduser"]) ? $_POST["iduser"]:'';
			 $this->retour['nom']=!empty($_POST["nom"]) ? $_POST["nom"]:''; 
			 $this->retour['nickname']=!empty($_POST["nickname"]) ? $_POST["nickname"]:'';
			 $this->retour['email']=!empty($_POST["email"]) ? $_POST["email"]:'';
			 $this->retour['passwd']=!empty($_POST["passwd"]) ? $_POST["passwd"]:'';
			 $this->retour['typeuser']=!empty($_POST["user"]) ? $_POST["user"]:'';

            echo "variable";
			
              $this->retour['valid'] = true;
              
			   UserDAO::inscrireUsager( $this->retour['id'],$this->retour['nom'],$this->retour['typeuser'], $this->retour['nickname'],$this->retour['passwd'],  $this->retour['email']);
			
		}

			public function getJson()
			{

			return $this->retour;

		    }

		
	}

?>