<?php 
	require_once("action/CommonAction.php");
	require_once("action/Dao/UserDAO.php");

	
	class ExtraireCitationsAction extends CommonAction
	{
       public $mauvaiseLogin = false;
       private static $PAGE_NAME = "ExtraireCitation";
       private $info;

       public function __construct()
       {
        parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGE_NAME);
       }

       protected function executeAction()
       {       
           $this->info = UserDAO::recuperercitation();  
       }

       public function getarray()
       {
          return $this->info;
       }

	}
	
?>