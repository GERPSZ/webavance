<?php
	

	session_start();
	abstract class CommonAction{

	public static $VISIBILITY_PUBLIC = 0;
	public static $VISIBILITY_PARENT = 1;
	public static $VISIBILITY_ADMIN   = 2;
	public static $VISIBILITY_PROPIETAIRE = 3;

	private $pageVisibility;
	public $pageName;

	public function __construct($pageVisibility,$pageName)
	{

       $this->pageVisibility = $pageVisibility;
       $this->pageName = $pageName;

	}


	public function get_name()
	{

       $nom = "Invite";

       if(isset($_SESSION["NOM"]))
       {
       	 $nom = $_SESSION["NOM"];
       }

       return $nom;
	}

    public function execute()
    {

      if(!empty($_GET["disconnect"]))
      {
      	session_unset();
      	session_destroy();
      	session_start();
      	header("location:index.php");
      }

      if(empty($_SESSION["visibility"]))
      {
         $_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
      }

      if($_SESSION["visibility"] < $this->pageVisibility)
      {
        header("location:index.php");
        exit;
      }

      $this->executeAction();

    }

    public function isLoggedIn()
    {
    	return $_SESSION["visibility"] >CommonAction::$VISIBILITY_PUBLIC;
    }
    
    protected function setUserInfo($info)
    {
    
      if(!empty($info))
      {  

        $typeuser = $info["TYPEUSER_GRIBOUILLE"];
        $visibility_session = UserDAO::retournervisibilite($typeuser);

         if($visibility_session > CommonAction::$VISIBILITY_PUBLIC)
         {
           $_SESSION["visibility"] = $visibility_session;
           $_SESSION["ID"] = $info["ID_USER"];
           $_SESSION["NOM"] = $info["NOM"];   
         }
  
         
      }

    }

    public function getCurrentPage() 
    {
      $pageURL = 'http://';
      if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
      } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
      }
      return $pageURL;
    }

    protected abstract function executeAction();
	
	}