<?php 
	require_once("action/CommonAction.php");
	require_once("action/Dao/UserDAO.php");

	
	class InsererCitationAction extends CommonAction
	{

       private static $PAGE_NAME = "Citation";
       public $mauvaiseLogin = false;
    
       public function __construct()
       {
          parent::__construct(CommonAction::$VISIBILITY_ADMIN,self::$PAGE_NAME);
       }          


       protected function executeAction()
 
       {
    
            $nomauteur =!empty($_POST["auteur"]) ? $_POST["auteur"]:"";
            $message =!empty($_POST["editor1"]) ? $_POST["editor1"]:"";

           if($nomauteur!='' and $message!='')
            UserDAO::InsererCitation($nomauteur,$message);
      

       }


	}
	
?>