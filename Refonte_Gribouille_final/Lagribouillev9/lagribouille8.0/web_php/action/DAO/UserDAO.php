<?php 
	require_once("Connexion.php");

	

	class UserDAO{
		
		
	public static function authenticate($nom,$mdp)
		 {
			$connexion = Connexion::getConnexion();

			{

				$sql = 'SELECT ID_USER,NOM,TYPEUSER_GRIBOUILLE,USERNAME,PASSWORD,EMAIL FROM USER_GRIBOUILLE WHERE NOM =: didv';

				$stdin = oci_parse($connexion,$sql);
				$didv = $nom;
				oci_bind_by_name($stdin, ':didv', $didv);
				$r = oci_execute($stdin);

				if(!$r)
				{
					$e = oci_error($stdin);
					trigger_error(htmlentities($e['pas de donnees'],ENT_QUOTES),E_USER_ERROR);
				}

                while (($row = oci_fetch_array($stdin,OCI_ASSOC+OCI_RETURN_LOBS))!=false)
                {	
                   $passwordbd = $row['PASSWORD'];
                   print_r($passwordbd);
                   print_r($nom);

                   if($mdp == $passwordbd)
                   {
                     echo "inside";
                     $info = $row;
                     print_r($info);
                   }
				
			    }
     
			}
                  return $info;
				
		}


	public static function retournervisibilite($typeuser)

	{

         $connexion = Connexion::getConnexion();
         
         $sql = 'SELECT VISIBILITY FROM TYPEUSER_GRIBOUILLE WHERE ID_TYPE =: didv';

         $stdin = oci_parse($connexion,$sql);
				 $didv = $typeuser;
				 oci_bind_by_name($stdin, ':didv', $didv);
				 $r = oci_execute($stdin);

				if(!$r)
				{
					$e = oci_error($stdin);
					trigger_error(htmlentities($e['pas de donnees'],ENT_QUOTES),E_USER_ERROR);
				}
       
                  while (($row = oci_fetch_array($stdin,OCI_ASSOC+OCI_RETURN_LOBS))!=false)
                {	
                   $visibility = $row['VISIBILITY'];
                   echo "la visibilite est ici";
                   print_r($visibility);
                   
                 }

                 return $visibility;  

	}

     public static function inscrireusager($identif, $name, $type, $username, $paswd, $mail)
     {
         
          $connexion = Connexion::getConnexion();
           

          
          $sql="INSERT INTO USER_GRIBOUILLE (ID_USER,NOM,TYPEUSER_GRIBOUILLE,USERNAME,PASSWORD,EMAIL) VALUES (:id,:nom,:typeuser,:name,:password,:email)";
          $st= oci_parse($connexion, $sql);
          
          $id = $identif;
          $nom = $name;
          $typeuser = $type;
          $name = $username;
          $password = $paswd;
          $email = $mail;

          oci_bind_by_name($st, ':id', $id);
          oci_bind_by_name($st, ':nom', $nom);
          oci_bind_by_name($st, ':typeuser', $typeuser);
          oci_bind_by_name($st, ':name', $name);
          oci_bind_by_name($st, ':password', $password);
          oci_bind_by_name($st, ':email', $email);
          
          oci_execute($st);
          
          oci_commit($connexion);   
           
     }

     public static function envoyerformulaire($namep,$chilname,$dnaic,$adpere,$cellpere,$celltravailpere,$telmaisonpere,$postalpere,$villepere,
                                              $emailpere,$ecpjour,$ecpsoir,$empcp,$namere,$admere,$cellmere,$celltravailmere,$telmaisonmere,
                                              $postalmere,$villemere,$emailmere,$ecpjourmere,$ecpsoirmere,$empcpmere)
     {
           
           $connexion = Connexion::getConnexion();
           $sqlform = "INSERT INTO FORMPARENTS (NAMEPERE,CHILD_NAME,BIRTH_DATE,ADRESSPERE,TELEPHONE_CELLPERE,TELEPHONE_TRAVAILPERE,
                                            TELEPHONE_MAISONPERE,COD_POSTALPERE,VILLEPERE,EMAILPERE,ETUDIANPERECEGEPJOUR,ETUDIANPERECEGEPSOIR,
                                            EMPLOYEPERECEGEP,NAMEMERE,ADRESSMERE,TELEPHONE_CELLMERE,TELEPHONE_TRAVAILMERE,TELEPHONE_MAISONMERE,
                                            COD_POSTALMERE,VILLEMERE,EMAILMERE,ETUDIANMERECEGEPJOUR,ETUDIANMERECEGEPSOIR,EMPLOYEMERECEGEP) 
                                            
                                            VALUES(:pere,:enfant,:dnaissance,:adresspere,:telpere,:celtpere,
                                                   :telmaipere,:pospere,:vpere,:mailpere,:ejourpere,:esoirpere,:emppere,
                                                   :mere,:adressmere,:telmere,:celtlmere,:telmaimere,:posmere,:vmere,:mailmere,
                                                   :ejourmere,:esoirmere,:empmere)";


          $statement= oci_parse($connexion, $sqlform);

          $pere = $namep;
          $enfant = $chilname;
          $dnaissance = $dnaic;
          $adresspere = $adpere;
          $telpere = $cellpere;
          $celtpere = $celltravailpere;
          $telmaipere = $telmaisonpere;
          $pospere = $postalpere;
          $vpere = $villepere;
          $mailpere = $emailpere;
          $ejourpere = $ecpjour;
          $esoirpere = $ecpsoir;
          $emppere = $empcp;

          $mere = $namere;
          $adressmere = $admere;
          $telmere = $cellmere;
          $celtlmere = $celltravailmere;
          $telmaimere = $telmaisonmere;
          $posmere = $postalmere;
          $vmere = $villemere;
          $mailmere = $emailmere;
          $ejourmere = $ecpjourmere;
          $esoirmere = $ecpsoirmere;
          $empmere = $empcpmere;



          oci_bind_by_name($statement, ':pere', $pere);
          oci_bind_by_name($statement, ':enfant', $enfant);
          oci_bind_by_name($statement, ':dnaissance', $dnaissance);
          oci_bind_by_name($statement, ':adresspere', $adresspere);
          oci_bind_by_name($statement, ':telpere', $telpere);
          oci_bind_by_name($statement, ':celtpere',$celtpere);
          oci_bind_by_name($statement, ':telmaipere', $telmaipere);
          oci_bind_by_name($statement, ':pospere', $pospere);
          oci_bind_by_name($statement, ':vpere', $vpere);
          oci_bind_by_name($statement, ':mailpere', $mailpere);
          oci_bind_by_name($statement, ':ejourpere', $ejourpere);
          oci_bind_by_name($statement, ':esoirpere', $esoirpere);
          oci_bind_by_name($statement, ':emppere', $emppere);

          oci_bind_by_name($statement, ':mere', $mere);
          oci_bind_by_name($statement, ':adressmere', $adressmere);
          oci_bind_by_name($statement, ':telmere', $telmere);
          oci_bind_by_name($statement, ':celtlmere', $celtlmere);
          oci_bind_by_name($statement, ':telmaimere', $telmaimere);
          oci_bind_by_name($statement, ':posmere', $posmere);
          oci_bind_by_name($statement, ':vmere', $vmere);
          oci_bind_by_name($statement, ':mailmere', $mailmere);
          oci_bind_by_name($statement, ':ejourmere', $ejourmere);
          oci_bind_by_name($statement, ':esoirmere', $esoirmere);
          oci_bind_by_name($statement, ':empmere', $empmere);
         

          $r = oci_execute($statement);
          
          oci_commit($connexion);  
        
     }

     public static function retourneremails()
     {

         $connexion = Connexion::getConnexion();
         
         $sql = 'SELECT EMAIL FROM USER_GRIBOUILLE WHERE TYPEUSER_GRIBOUILLE=: didv';

         $stdin = oci_parse($connexion,$sql);
         $didv = 3;
         oci_bind_by_name($stdin, ':didv', $didv);
         $r = oci_execute($stdin);

         $arrayemails = array();
       
                  while (($row = oci_fetch_array($stdin,OCI_ASSOC+OCI_RETURN_LOBS))!=false)
                {   
                    $emailuser = $row['EMAIL']; 
                    $arrayemails[] = $emailuser;
                  
                 }

                 return $arrayemails;
     }
   

     public static function insererCitation($auteur,$message)

     {

           $connexion = Connexion::getConnexion();

           $sql = "INSERT INTO MESSAGE_ENFANTS (COMMENTAIRES,AUTEUR) VALUES (:comments,:signature)";
        
           $statement = oci_parse($connexion, $sql);
            
           $comments = $message;
           $signature = $auteur;

         

            oci_bind_by_name($statement, ':comments', $comments);
            oci_bind_by_name($statement, ':signature', $signature);

            $r = oci_execute($statement);

           
            oci_commit($connexion);  
         

     }


     public static function recuperercitation()
     
     {

           $connexion = Connexion::getConnexion();
         
           $sql = "SELECT COMMENTAIRES,AUTEUR FROM MESSAGE_ENFANTS";
           $stdin = oci_parse($connexion,$sql);
           $r = oci_execute($stdin);

             $arraymessageassoc = array();
       
                  while (($row = oci_fetch_array($stdin,OCI_ASSOC+OCI_RETURN_LOBS))!=false)
                {   
                    $commentaires =!empty($row['COMMENTAIRES']) ? $row['COMMENTAIRES']:''; 
                    $auteur = !empty($row['AUTEUR']) ? $row['AUTEUR']:'';
                    
                    if($commentaires!='' and $auteur!='')
                      {
                       array_push($arraymessageassoc,array($row['COMMENTAIRES'] => $row['AUTEUR']));     
                      }
                }
         
            return $arraymessageassoc;
        }


    public static function recupererimage()
    {

          $connexion = Connexion::getConnexion();
          
           $sql = "SELECT CHEMIN FROM IMAGE";
           $stdin = oci_parse($connexion,$sql);
           $r = oci_execute($stdin);

             $arraychemin = array();
       
                  while (($row = oci_fetch_array($stdin,OCI_ASSOC+OCI_RETURN_LOBS))!=false)
                {   
                    $route = $row['CHEMIN']; 
                    $arraychemin[] = $route;
                         
                }
         

            return $arraychemin;


    }



   public static function inserermessage($communication)

     {

           $connexion = Connexion::getConnexion();

           $sql = "INSERT INTO MESSAGEPARENTS (MESSAGES) VALUES (:comments)";
        
           $statement = oci_parse($connexion, $sql);
            
           $comments = $communication;
          

            oci_bind_by_name($statement, ':comments', $comments);
           

            $r = oci_execute($statement);

           
            oci_commit($connexion);  
         

     }

     public static function recuperermessage()
    {

          $connexion = Connexion::getConnexion();
          
           $sql = "SELECT MESSAGES FROM MESSAGEPARENTS";
           $stdin = oci_parse($connexion,$sql);
           $r = oci_execute($stdin);

             $arraychemin = array();
       
                  while (($row = oci_fetch_array($stdin,OCI_ASSOC+OCI_RETURN_LOBS))!=false)
                {   
                    $route = $row['MESSAGES']; 
                    $arraychemin[] = $route;
                         
                }
         

            return $arraychemin;
    }

    public static function recIdcheminpage()
      {

           $connexion = Connexion::getConnexion();
         
           $sql = "SELECT IDPAGE,CHEMINPAGE FROM EDITEUR_PAGES";

           $stdin = oci_parse($connexion,$sql);
           $r = oci_execute($stdin);

           $arrayidpage = array();
       
                  while (($row = oci_fetch_array($stdin,OCI_ASSOC+OCI_RETURN_LOBS))!=false)
                {   
                    $idpage =!empty($row['IDPAGE']) ? $row['IDPAGE']:''; 
                    $cheminpage = !empty($row['CHEMINPAGE']) ? $row['CHEMINPAGE']:'';
                    
                    if($idpage!='' and $cheminpage!='')
                      {
                       array_push($arrayidpage,array($row['IDPAGE'] => $row['CHEMINPAGE']));     
                      }
                }
         
            return $arrayidpage;
        }


    public static function InsererClob($route,$cleb)  
    {
  
           $connexion = Connexion::getConnexion();

           $sql = 'UPDATE EDITEUR_PAGES SET TEXTE=:information WHERE CHEMINPAGE=:id';

         
           $statement = oci_parse($connexion, $sql);

            $id = $route;

            $information = $cleb;

            oci_bind_by_name($statement, ':information', $information);
            oci_bind_by_name($statement, ':id', $id);

            $r = oci_execute($statement);
  
            oci_commit($connexion);  
         

    }



    }
	   