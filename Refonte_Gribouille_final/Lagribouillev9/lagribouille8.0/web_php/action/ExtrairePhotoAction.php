<?php 
	require_once("action/CommonAction.php");
	require_once("action/Dao/UserDAO.php");

	
	class ExtrairePhotoAction extends CommonAction
	{
       public $mauvaiseLogin = false;
       private static $PAGE_NAME = "PHOTO";

      public function __construct()
      {
        parent::__construct(CommonAction::$VISIBILITY_PARENT,self::$PAGE_NAME);
       }          

        protected function executeAction()
       {       
           $this->info = UserDAO::recupererimage();  
       }

       public function getarray()
       {
          return $this->info;
       }


	}
	
?>