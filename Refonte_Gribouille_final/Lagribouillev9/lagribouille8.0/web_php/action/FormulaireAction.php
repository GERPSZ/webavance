<?php 
	require_once("action/CommonAction.php");
	require_once("action/Dao/UserDAO.php");

	
	class FormulaireAction extends CommonAction
   {
    
    private static $PAGENAME = "Formulaire";

    public function __construct()
    {
        parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGENAME);
    }

    protected function executeAction()

    {

            $nompere=!empty($_POST["namepere"]) ? $_POST["namepere"]:"";
            $celpere=!empty($_POST["telephonecel"]) ? $_POST["telephonecel"]:"";
            $teltravailpere=!empty($_POST["telephonetravail"]) ? $_POST["telephonetravail"]:"";
            $telemaisonpere=!empty($_POST["telephonemaison"]) ? $_POST["telephonemaison"]:"";
            $cpostalpere=!empty($_POST["postal"]) ? $_POST["postal"]:"";
            $villepere=!empty($_POST["ville"]) ? $_POST["ville"]:"";
            $emailpere=!empty($_POST["email"]) ? $_POST["email"]:"";
            $addpere=!empty($_POST["adresse"]) ? $_POST["adresse"]:"";


            if(!empty($_POST["etudiantjour"]))
            {
                 $etujourpere = 1;
            }
            else
            {
                 $etujourpere = 0;
            }

             if(!empty($_POST["etudiantsoir"]))
            {
                 $etusoirpere = 1;
            }
            else
            {
                 $etusoirpere = 0;
            }

            

             if(!empty($_POST["employecvm"]))
            {
                 $empcgpere = 1;
            }
            else
            {
                 $empcgpere = 0;
            }

            
            $nommere=!empty($_POST["namemere"]) ? $_POST["namemere"]:"";
            $celmere=!empty($_POST["telephonecelmere"]) ? $_POST["telephonecelmere"]:"" ;
            $teltravailmere=!empty($_POST["telephonetravailmere"]) ? $_POST["telephonetravailmere"]:"";
            $telemaisonmere=!empty($_POST["telephonemaisonmere"]) ? $_POST["telephonemaisonmere"]:"";
            $cpostalmere=!empty($_POST["codpostalmere"]) ? $_POST["codpostalmere"]:"";
            $villemere=!empty($_POST["villemere"]) ? $_POST["villemere"]:"";
            $emailmere=!empty($_POST["emailmere"]) ? $_POST["emailmere"]:"";
            $addmere=!empty($_POST["adressemere"]) ? $_POST["adressemere"]:"";


            if(!empty($_POST["etudiantjourmere"]))
            {
                 $etujourmere = 1;
            }
            else
            {
                 $etujourmere = 0;
            }

             if(!empty($_POST["etudiantsoirmere"]))
            {
                 $etusoirmere = 1;
            }
            else
            {
                 $etusoirmere = 0;
            }

           

             if(!empty($_POST["employecvmmere"]))
            {
                 $empcgmere = 1;
            }
            else
            {
                 $empcgmere = 0;
            }

            $nomenfant=!empty($_POST["namenfant"]) ? $_POST["namenfant"]:"";
            $jour = !empty($_POST["dateJour"]) ? $_POST["dateJour"]:"";
            $mois = !empty($_POST["dateMois"]) ? $_POST["dateMois"]:"";
            $an   = !empty($_POST["dateAnnee"]) ? $_POST["dateAnnee"]:"";
            $datenfant = $jour."-".$mois."-".$an;
            
         
         
            
            UserDAO::envoyerformulaire($nompere,$nomenfant,$datenfant,$addpere,$celpere,$teltravailpere,$telemaisonpere,$cpostalpere,$villepere,
                                              $emailpere,$etujourpere,$etusoirpere,$empcgpere,$nommere,$addmere,$celmere,$teltravailmere,$telemaisonmere,
                                              $cpostalmere,$villemere,$emailmere,$etujourmere,$etusoirmere,$empcgmere);
        
        
    }

    
  }
	
?>