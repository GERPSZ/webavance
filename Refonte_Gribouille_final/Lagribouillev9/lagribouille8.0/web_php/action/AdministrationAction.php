<?php 
	require_once("action/CommonAction.php");
	require_once("action/DAO/UserDAO.php");


	class AdministrationAction extends CommonAction
	{
		
		private static $PAGENAME = "Administration";
		public $mauvaiseLogin = false;
		
		public function __construct()
		{
	    	parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGENAME);
		}          

		protected function executeAction()
		{
	     

             if(!empty($_POST['name'])){
             $info = UserDAO::authenticate($_POST["name"],$_POST["password"]);

             if(isset($info))
             {
             	parent::setUserInfo($info);
             
             }
             else
             {
             	$this->mauvaiseLogin = true;
             }
         }
       
		}

	}