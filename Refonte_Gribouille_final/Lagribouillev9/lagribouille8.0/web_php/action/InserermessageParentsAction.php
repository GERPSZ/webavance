<?php 
	require_once("action/CommonAction.php");
	require_once("action/Dao/UserDAO.php");

	
	class InserermessageParentsAction extends CommonAction
	{

       private static $PAGE_NAME = "Citation";
       public $mauvaiseLogin = false;
    
       public function __construct()
       {
          parent::__construct(CommonAction::$VISIBILITY_ADMIN,self::$PAGE_NAME);
       }          


       protected function executeAction()
 
       {
    
            $message = !empty($_POST["comment"]) ? $_POST["comment"]:"";
           
            UserDAO::inserermessage($message);
      
       }


	}
	
?>