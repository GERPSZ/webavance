<?php

/* -----------------------------------------
 *                                        *
 *    Projet lagribouille : H2016         *
 *    Fait Par : GPS                         *
 *                                         *
 *---------------------------------------- */

    require_once("partial/header2.php");
  	
?>

 
			<div class="main">            
      <div class="maincontent servicesofferts">
        
          
      <h1>Services offerts<br />
	    <br />
	    </h1>


    <ul>
      <li>Un service de garde &eacute;ducatif pour huit (8) enfants &acirc;g&eacute;s de 30 mois &agrave; 5 ans et inscrits &agrave; temps plein. </li>
      <li>La priorit&eacute;  est accord&eacute;e aux parents &eacute;tudiant au CVM. Les autres places sont allou&eacute;es aux parents travaillant au CVM. </li>
      <li>Les frais de garde sont de 7&nbsp;$ par jour, cela inclut deux  collations et un repas complet et &eacute;quilibr&eacute;. </li>
      <li>La Gribouille est ouverte du lundi au vendredi de 7&nbsp;h&nbsp;45 &agrave; 17&nbsp;h&nbsp;30. </li>
      <li>Le service de garde est ouvert durant les p&eacute;riodes de cours seulement, soit de la semaine&nbsp;1 &agrave; 15, et ce, aux sessions d&rsquo;automne et d&rsquo;hiver, &agrave; l&rsquo;exception des jours de cong&eacute; pr&eacute;vus au calendrier scolaire. </li>
      <li>Pour inscrire le nom de votre enfant sur la liste d&rsquo;attente. Il suffit de remplir le  <a href="formulaireinscription.php" target="_blank" class="hyper"><strong>formulaire en ligne</strong></a> (PDF), et une personne communiquera avec vous le moment venu. </li>
    </ul>
    <h3>La Gribouille, c&rsquo;est aussi&nbsp;:</h3>
    <ul>
      <li>Un centre de recherche et d&rsquo;observation  pour les parents, les &eacute;tudiants et les enseignants en Techniques d&rsquo;&eacute;ducation &agrave; l&rsquo;enfance et en Techniques d&rsquo;&eacute;ducation sp&eacute;cialis&eacute;e;</li>
      <li>Une ressource p&eacute;dagogique extraordinaire pour les &eacute;tudiants et les       enseignants du c&eacute;gep du Vieux Montr&eacute;al; </li>
      <li>Une ressource p&eacute;dagogique indispensable pour tout &eacute;tudiant qui se destine &agrave; l&rsquo;intervention aupr&egrave;s des jeunes enfants;</li>
      <li>Un laboratoire d&rsquo;observation de qualit&eacute; situ&eacute; au local A8.56;</li>
      <li>Un milieu de vie &eacute;ducatif pour les enfants et le personnel &eacute;ducateur situ&eacute; au local A8.57; </li>
      <li>Un programme d&rsquo;activit&eacute; qui r&eacute;pond aux besoins et &agrave; la curiosit&eacute; des enfants, sans oublier les besoins des &eacute;tudiants et des enseignants; </li>
      <li>Un service de garde de qualit&eacute; o&ugrave; l'on applique le programme &eacute;ducatif des CPE; </li>
      <li>Un accueil chaleureux pour chaque parent et pour chaque enfant;</li>
      <li>Un lieu qui permet aux parents de se concentrer sur leur travail scolaire, compte tenu de la qualit&eacute; du service offert aux enfants et de l&rsquo;horaire de La Gribouille; </li>
      <li>Un lieu o&ugrave; les parents peuvent d&eacute;velopper ou obtenir du soutien en mati&egrave;re de comp&eacute;tences parentales; </li>
      <li>Un lieu o&ugrave; le parent peut participer &agrave; la vie de son enfant dans le cadre de    certaines activit&eacute;s pr&eacute;vues au calendrier, telles que des repas       communautaires, des f&ecirc;tes et des sorties. </li>
    </ul>

   
    <p>&nbsp;</p>
    <p style="text-align:center;">&nbsp;</p>
    
              
                  	
      </div>
      </div>
            
                
			
                
			</div><!-- /main -->
                 
	     
        
		<script src="../js/classie.js"></script>
        

        
	</body>
</html>

<?php




?>