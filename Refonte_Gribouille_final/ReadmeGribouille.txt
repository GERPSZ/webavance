Refonte du site web : La Gribouille 
1.	Tables dans la BD Oracle du CVM.
USER_GRIBOUILLE
TYPEUSER_GRIBOUILLE
FORMPARENTS
MESSAGE_ENFANTS
IMAGE
MESSAGE_PARENTS
EDITEUR_PAGES

2.	Authentification : Super Admin
Nom : GPS
Mot de passe : AAAaaa111 (toujours 3 majuscules 3 minuscules et trois chiffres)

3.	Le menu change selon le type d�authentification
   Il faut se loger comme super admin pour voir tous les options de l�admin

4.	Page d�authentification : administration. PHP

5.	Modification des citations : Citations ( citations.PHP). Il faut remplir le champ auteur et le message qui va �tre publi� sur les pages de la site. Les citations sont sauvegardes sur la table MESSAGE_ENFANTS.

6.	Les citations changent de fa�on al�atoire sur chaque page du site.

7.	Modification du contenu des pages : Formulaires (editeur-pages.PHP) : Tout le texte est sauvegarde dans la BD Oracle dans la table EDITEUR_PAGES. De plus, il est possible de visualiser l�aspect final de la page sur tampon. PHP.

Processus.
Il faut choisir sur le combo box  la page � modifier (lequel sera rempli � partir de la BD) et automatiquement le contenu de la page apparait sur le CK Editor. Une fois que on finit de modifier les pages, il faut cliquer sur sauvegarder pour cr�er la page tampon et envoyer le texte vers la BD.

8.	Acc�s au formulaire envoy�s : Toute l�information est sauvegarde dans la BD, dans la table FORMPARENTS. De la c�te de l�administrateur il pourrait voir tout le contenu de la page plus les options �tudiant jour/�tudiant soir/employe_cvm (champs binaires 1-0).

9.	Envoyer emails : email (envoyeremail.PHP). Il faut �crire le sujet et le contenu. Toute l�information est envoy�e vers la Table MESSAGE_PARENTS. Un email est envoy� � chaque utilisateur qui est identifi� comme type d�usager parent. De plus le message est enregistr� sur la page Parents.

10.	Nouveaux Membres : Membres (ajoutermembre.php) : En utilisant cette fonction l�administrateur peut ajouter un nouvel usager sur la table USER_GRIBOUILLE.
 
Types d�utilisateur :

1	PROPIETAIRE
2	ADMIN
3	PARENT
4	PUBLIC

Note : ICI j�utilise AJAX pour conserver les donn�es. Note 5.


11.	Formulaire d�inscription : Inscription(FormulaireInscription.php) :Le formulaire pour les deux parents est pr�sente dans la site web. Toute la information est envoy�e vers la table FORMPARENTS.  Tous les champs du document papier sont inclus.
      Note : Ici j�utilise SECURECAPTCHA pour confirmer l�envoie de l�information. 4.a.

12.	 Section parent : Espace Parents(pageparents.php). Les photos sauvegard�s dans la table IMAGE(src) de la BD Oracle et les messages envoy�s par email aux parents sont affich�s.


13.	 Notes :
Protection contre les injections. Bind variables pour chaque fonction dans le DAO qui a acc�s � la BD.

CKEDITOR a �t� utilis� pour �diter le contenu de pages et les messages des enfants.(Citations.PHP).
Effets javascript pour les animations.

Htaccess pour les pages 404.500.

Pour sauvegarder le contenu des pages un champ de type CLOB est utilis� pour avoir acc�s � plus de 4000 caract�res.


 


  




 
