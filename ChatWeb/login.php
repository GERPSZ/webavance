<?php
	require_once("action/LoginAction.php");

	$action = new LoginAction();
	$action->execute();

	require_once("partial/header.php");
?>

	<h1>Connexion</h1>

	<div class="login-form-frame">
		<form action="login.php" method="post">
			
            
          

			<div class="form-label">
				<label for="username">username : </label>
			</div>
			<div class="form-input">
				<input type="text" name="username" id="username" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				<label for="password">password : </label>
			</div>
			<div class="form-input">
				<input type="password" name="password" id="password" />
			</div>
			<div class="form-separator"></div>

			
			<div class="form-label">
				&nbsp;
			</div>
			<div class="form-input">
				<input type="submit" value="submit">
			</div>
			<div class="form-separator"></div>
		</form>
	</div>

