<!DOCTYPE html>
<html lang="fr">
    <head>
        <link href="css/global.css" rel="stylesheet" />
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<title>StarCVM coffees</title>
		<meta charset="utf-8" />
    </head>
    <body>
		<div class="header">
				<div class="site-title-section">
					<h2>StarCVM Coffees</h2>
				</div>
				<div class="username-section">
					Bonjour, 
					<?php 
						echo $action->getUsername();
					?>
					!
				</div>
				<div class="clear"></div>
				
				<div class="menu">
					<ul>
						<li><a href="index.php">Accueil du site</a></li>
						<?php 
							if ($action->isLoggedIn()) {
								?>
								<li><a href="home.php">Mon accueil perso</a></li>
								<li><a href="profile.php">Mon profil</a></li>
								<li><a href="?logout=true">Déconnexion</a></li>
								<?php
							}
							else {
								?>
								<li><a href="login.php">Se connecter</a></li>
								<?php
							}
						 ?>
						 <li><a href="contact.php">Contact</a></li>
					</ul>
				</div>
			</div>
	
	    <div id="liste"> </div>
		<div id="container">
			
		