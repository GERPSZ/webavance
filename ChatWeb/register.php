<?php
	require_once("action/RegisterAction.php");
    
    $action = new RegisterAction();
	$action->execute();

	require_once("partial/header.php");
?>

	<h1>Connexion</h1>

	<div class="login-form-frame">
		<form action="register.php" method="post">
		
            <div class="form-label">
				<label for="Matricule">NO : </label>
			</div>
			<div class="form-input">
				<input type="text" name="NO" id="NO" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				<label for="firstname">firstName : </label>
			</div>
			<div class="form-input">
				<input type="text" name="firstname" id="firstname" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				<label for="lastname">lastName : </label>
			</div>
			<div class="form-input">
				<input type="text" name="lastname" id="lastname" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				<label for="username">Nom d'usager : </label>
			</div>
			<div class="form-input">
				<input type="text" name="username" id="username" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				<label for="password">Mot de passe : </label>
			</div>
			<div class="form-input">
				<input type="password" name="password" id="password" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				<label for="welcome">WelcomeText : </label>
			</div>
			<div class="form-input">
				<input type="text" name="welcome" id="welcome" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				&nbsp;
			</div>
			<div class="form-input">
			    <input type="submit" value="Register">	
			</div>
			<div class="form-separator"></div>
		</form>
	</div>
