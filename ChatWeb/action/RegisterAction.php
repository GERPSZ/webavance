<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/UserDAO.php");
	
	class RegisterAction extends CommonAction {
	##	public $wrongLogin = false;
##		public $wasDenied = false;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

        public function callApi($url, $service, array $data) 
			{
                $data = json_encode($data);			// Encodage des données/paramètres
	            $data = urlencode($data);			// Encodage pour être un URL valide
                
                echo $url;
// Appel au serveur et décodage de la réponse
	            return json_decode(file_get_contents($url . "/" . $service . "?data=" . $data)); 
            }	

		protected function executeAction() 
		{
			
			if (!empty($_POST["NO"])){
            	 $data = array();
          
                $data["no"] = $_POST["NO"];
                $data["firstName"] = $_POST["firstname"];
                $data["lastName"] = $_POST["lastname"];
                $data["username"] = $_POST["username"];
                $data["password"] = md5($_POST["password"]);
                $data["welcomeText"] = $_POST["welcome"]; 

                    
                $resultat = $this->callApi("http://apps-de-cours.com/web-chat/server/api","register",$data);

                 echo $resultat;


            }

		}

	}

  