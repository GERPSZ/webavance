<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/UserDAO.php");
	
	class LoginAction extends CommonAction {
	##	public $wrongLogin = false;
##		public $wasDenied = false;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

        public function callApi($url, $service, array $data) 
			{
                $data = json_encode($data);			// Encodage des données/paramètres
	            $data = urlencode($data);			// Encodage pour être un URL valide
                
                echo $url;
// Appel au serveur et décodage de la réponse
	            return json_decode(file_get_contents($url . "/" . $service . "?data=" . $data)); 
            }	

		protected function executeAction() 
		{
			
			if (!empty($_POST["username"])){
            	$visibility = UserDAO::login($_POST["username"],$_POST["password"]);

            	if($visibility>CommonAction::$VISIBILITY_PUBLIC)
            	{
            		$_SESSION["visibility"] = $visibility;
            		$_SESSION["username"] = $_POST["username"];
            		header("location:home.php");
            		exit;
            	}

            	else
            	{
            		$this->wrongLogin = TRUE;
            	}
            }

		}

	}

  



 