<?php
	require_once("action/GetResultAction.php");
	$action = new GetResultAction();
	
	$action->execute();

	echo json_encode($action->getJson());