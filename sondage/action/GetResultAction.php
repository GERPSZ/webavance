<?php
	require_once("action/DAO/QuestionDAO.php");

	class GetResultAction {
		public $result;
	
		public function __construct() {
		
		}
		
		public function execute() 
		{
			$question =(!empty($_GET["question"]) ? $_GET["question"] : null);
			
			if (strlen($question) > 0 && strpos($question, "?") === strlen($question) - 1) {
				$this->result = QuestionDAO::getResult();
			}
			else {
				$this->result = "INVALID_QUESTION";
			}
			
		}


		public function getJson()
		{
			return $this->result;
		}

	}
