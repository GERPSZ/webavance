function sendQuestion() {
	var question = document.getElementById('question').value;
	document.getElementById('question').value = "";
	
	if (question.length > 2) {
		send(question);
	}
}

function send(question) {
	// Faire une requ�te AJAX (un appel � PHP) et appeler la fonction afficherResultat 
	// avec le tableau r�sultat en param�tre
}

// ==============================================================
// Ne pas modifier ce qui suit
function afficherResultat(tableauResultat) {
	document.getElementById("container").innerHTML = "";
	setTimeout(function () {animate(tableauResultat)}, 300);
}

function animate(tableauResultat) {

	if (tableauResultat != "INVALID_QUESTION") {
		for (var key in tableauResultat) {
			var element = document.createElement("div");
			element.setAttribute("class", "activityBar");
			element.style.width = (tableauResultat[key] * 4 + 60) + "px";
			element.innerHTML = key + " <i>(" + tableauResultat[key] + "%)</i>";
			document.getElementById("container").appendChild(element);
			delete tableauResultat[key];
			setTimeout(function () {animate(tableauResultat)}, 300);
			break;
		}
	}
}