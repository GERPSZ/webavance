
<?php 
	



?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Questionnaire Live !</title>
        <script src="js/javascript.js"></script>
        <script src="js/jquery.js"></script>
		<link href="css/global.css" rel="stylesheet"/>
    </head>
    <body>

		<div class="site">
			<div class="title">[ Questions/R�ponses des profs ]</div>
			<div class="container" id="container"></div>
			<ul id="liste"></ul>
			<div class="questionContainer">
				<textarea id="question" cols="60" rows="2" style="width:492px"></textarea>
				<div class="buttonContainer">
					<input type="button" value="Demander" onclick="sendQuestion()"/>
				</div>
			</div>
		</div>
	</body>
</html>

<script>

    function sendQuestion(){
    	$.ajax({
			type: 'GET', 				// ou POST
			url: 'get-result.php',		// URL � appeler
			data: {						// les donn�es du formulaire (si vide, ne pas mettre data)
				question :  $('#question').val()
			}
		})
		.done(function(data) {
		
			data = JSON.parse(data); 	// d�s�rialisation/reconstruction de la r�ponse (ici, c'est seulement un chiffre)
			console.log(data);
				$('#container').text(JSON.stringify(data));		
		});
    }
    </script>