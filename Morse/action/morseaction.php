

<?php

 require_once("utils/morse-utils.php");
 
 function execute()
 {
    $showerror = false;
    
    if(!empty($_POST["textareamorse"]))
    {
       $variabletemp = $_POST["textareamorse"];
       $morsetext = encodeMorse($variabletemp);
       echo $morsetext;
    }
    else
    {
        if(!empty($_POST["textareascii"]))
        {
            $variabledeco = $_POST["textareascii"];
            $textmessage = decodeMorse($variabledeco);
            echo $textmessage;
        }

    	$showerror = true;
    }
    
 	return $showerror;
 }    
