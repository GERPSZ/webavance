<?php

namespace App\Http\Controllers;

class ForumController extends Controller
{
    private function loadSubjects() {
        $subjects = [];
        $subjects[] = array(1, "Web");
        $subjects[] = array(2, "DBA");
        $subjects[] = array(3, "Linux");

        return $subjects;
    }

    public function showSubjects() {
        $subjectList = $this->loadSubjects();

        return view('forum.subjects', ["subjects" =>  $subjectList ]);
    }
}
