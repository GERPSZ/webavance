<?php 
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();	
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Sha1</title>
        <link href="css/global.css" rel="stylesheet" />
        <script src="http://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
        <meta charset="utf-8"/>
    </head>
    <body>
		<div class="sectionKey"></div>
		<div class="sectionAuthentification">
			<h1>
				Sha1 Converter
			</h1>

			<div class="formLabel"><label for="texte"> Texte : </label></div>
			<div class="formInput"><input type="text" name="texte" id="texte" /></div>
			<div class="formSeparator"></div>
			<input type="button" value="Demander" onclick="getMDP()"/>
		</div>
			<div id="result" style="text-align:center;color:white;font-size:40px"></div>
    </body>
</html>
    <script>
    function getMDP(){
    	$.ajax({
			type: 'POST', 				// ou POST
			url: 'converter.php',		// URL à appeler
			data: {						// les données du formulaire (si vide, ne pas mettre data)
				mdp :  $('#texte').val()
			}
		})
		.done(function(data) {
			//console.log(data)
			data = JSON.parse(data); 	// désérialisation/reconstruction de la réponse (ici, c'est seulement un chiffre)
			if(data['valid']){
				$('#result').text( data['clef'] );
			}			
		});
    }
    </script>


