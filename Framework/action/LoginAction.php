<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/UserDAO.php");
	
	class LoginAction extends CommonAction {
		public $wrongLogin = false;
		public $wasDenied = false;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if (!empty($_GET["login-error"])) {
				$this->wasDenied = true;
			}

			if (isset($_POST["username"])) {
				$visibility = UserDAO::login($_POST["username"], $_POST["pwd"]);

				if ($visibility > CommonAction::$VISIBILITY_PUBLIC) {

					$_SESSION["visibility"] = $visibility;
					$_SESSION["username"] = $_POST["username"];

					header("location:home.php");
					exit;
				}
				else {
					$this->wrongLogin = true;
				}
			}
		}
	}
