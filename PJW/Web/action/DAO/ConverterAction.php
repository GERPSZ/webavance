<?php 
require_once("action/CommonAction.php");

	class ConverterAction extends CommonAction {
		private $retour = array();

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}
	
		protected function executeAction() {
			$this->retour['valid'] = false;	
			if (!empty($_POST["mdp"])){
				$this->retour['clef'] =  password_hash($_POST["mdp"], CRYPT_BLOWFISH);
				$this->retour['valid'] = true;
				//pour décrypter 
				//http://php.net/manual/en/function.password-verify.php
				//boolean password_verify ( string $password , string $hash )
			}
		}

		public function getJson(){
			return $this->retour;
		}
	}

	

