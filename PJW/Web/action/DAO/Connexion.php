<?php 

	class Connexion
	 {

		private static $connexion = null;

		// singleton
		public static function getConnexion() 
		{
			if (empty(Connexion::$connexion)) 
			{
				Connexion::$connexion = oci_connect("e1301184", "A", "//10.57.4.60/DECINFO.edu");
				//permet de lancer les erreurs
				if(!(Connexion::$connexion))
				 {
                    $m = oci_error();
                    echo $m['message'], "\n";
                    exit;
                 }
                 else 
                 {
                    print "Connected to Oracle";
                 }

			}
			return Connexion::$connexion;
		}


		/* Pas nécessaire car quand on ferme la page, la connexion est fermée aussi*/
		public static function closeConnexion()
		 {
			if (!empty(Connexion::$connexion)) 
			{	
				oci_close(Connexion::$connexion);
			}
		}
	}

 
