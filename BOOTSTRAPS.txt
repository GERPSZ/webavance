active class

The active class is applied to the navigation element the user is currently viewing.

In the case of your given code, the user is viewing the the profile. It will serve as a guide or reminder to where in the website the visitor is in. That is why the active class is applied, which comes handy when viewing a website with many navigation links.


The .slide class adds a CSS transition and animation effect, which makes the items slide when showing a new item. Omit this class if you do not want this effect.