<?php 

	session_start();

	abstract class CommonAction {
		public static $VISIBILITY_PUBLIC = 0;
		public static $VISIBILITY_MEMBER = 1;
		public static $VISIBILITY_MODERATOR = 2;
		public static $VISIBILITY_ADMINISTRATOR = 3;

		private $pageVisibility;
		private $titre;

		public function __construct($pageVisibility, $titre) {
			$this->pageVisibility = $pageVisibility;
			$this->titre = $titre;
		}

		public function execute() {
			if (!empty($_GET["disconnect"])) {
				session_unset();
				session_destroy();
				session_start();
			}

			if (empty($_SESSION["visibility"])) {
				$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
			}

			if ($_SESSION["visibility"] < $this->pageVisibility)	{
				header("location:login.php");
				exit;
			}

			// francais/anglais
			// compteur de pages

			$this->executeAction();
		}

		public function getName() {
			$name = "ami";

			if ($_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC) {
				$name = $_SESSION["name"];
			}

			return $name;
		}

		public function isLoggedIn() {
			return $_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC;
		}

		public function getPageTitle(){
					return $this->titre;
				}
		protected abstract function executeAction();

	}