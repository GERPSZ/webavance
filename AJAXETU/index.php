<?php 
	require_once("action/indexAction.php");
	$action = new indexAction();
	$action->execute();

	require_once("partial/header.php");
 ?>

	<h1>Lumières Montréal</h1>

	<div id=info>
		<p>Nous voulons que la pollution lumineuse soit toujours présente et les spectres de l'ombre loin de notre île.</p>
		<p>Signalez en toute sécurité les lampadaires défextueux en nous envoyant un numéro de porte valide.</p>
	</div>
	<div id="message"></div>
	<div id="formulaire">
		<form onsubmit="return validation();">

			<label for="noPorte">Numéro de porte</label>
	  		<input type="text" name="noPorte" id="noPorte">
	  			<div id=spacer></div>
	  		<label for="prenom">Votre Prénom</label>
	  		<input type="text" name="prenom" id="prenom">
	  			<div id=spacer></div>
	  		<label for="noSerie">Numéro de série deu lampadaire</label>
	  		<input type="text" name="noSerie" id="noSerie">
	  			<div id=spacer></div>
	  		<label for="chien">Nom de votre chien</label>
	  		<input type="text" name="chien" id="chien">
	  			<div id=spacer></div>
	  			<input type="submit" name="launch" value="Envoyer"></input>
	  		
		</form>
	</div>

<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.0.min.js"></script>
<!-- avantage : loadé de pas mon serveur, mais du serveur le plus près, plus rapide donc et moins de bande passante pour moi!d -->
<script>
	function validation()
	{
		valid=false;
		$.ajax({
			url: "indexAjax.php",
			type: 'POST', 
			data: { Clef: "value"
				}
			})
			.done(function(data) {
				data = JSON.parse(data);
			}).fail(function(data) {
				alert("failed, big time!")
			}).always(function(data) {
				// Always called, even it fails
			});	;
		return valid;//prévient l'envoie du formulaire
	}
</script>
