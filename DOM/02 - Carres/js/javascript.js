var spriteList = [];

document.onmousemove = function (e)
{
   
  spriteList.push(new Carre(e.pageX,e.pageY));
  
}

window.onload = tick;

function tick()
{


  for(var i=0; i< spriteList.length; i++)
  {
     var alive = spriteList[i].tick();

     if(!alive)
     {
        spriteList.splice(i,1);
        i--;
     }


  }

   window.requestAnimationFrame(tick);
}

function Carre(x, y) {
	this.x = x;
	this.y = y;
   this.speed = 1;
   this.velocity = 0.2;
	
	this.div = document.createElement("div");
   this.div.setAttribute("class","square");
   this.div.style.left = this.x + "px";
   this.div.style.top = this.y + "px";

   document.body.appendChild(this.div);
}

Carre.prototype.tick = function()
 {
   
   var alive = this.y>0 ? true : false;
   
   this.speed += this.velocity;
   this.y -= this.speed;

   this.div.style.top = this.y + "px";

   if(!alive)
   {
   document.body.removeChild(this.div);
   }


   return alive;

};